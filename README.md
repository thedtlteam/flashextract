# README #

Implementation of FlashExtract

To run:
>node dsltext.js

More info on the directories:
- /debug
    Holds all the text files that are useful for debugging
- /learn
    Holds all the files for learning programs
- /position
    Holds all files related to Position, i.e. AbsPos, RegPos, etc
- /program
    Contains all Program objects, i.e. FilterBoolProgram, etc
- /regex
    Objects that hold regex/tokens
- /region
    Objects used to hold information about the document and positive/negative examples
- /UI
    User interface
- /utils
    Misc. tasks/objects