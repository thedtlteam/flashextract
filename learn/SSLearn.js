var _ = require('lodash');

var PairLearn = require('./PairLearn.js');
var PSLearn = require('./PSLearn.js');
var LSLearn = require('./LSLearn.js');

var FilterIntProgram =  require("../program/FilterIntProgram.js") ;
var LinesMapPSProgram =  require("../program/LinesMapPSProgram.js") ;
var MapSSProgram = require("../program/MapSSProgram.js") ;
var Position = require("../position/Position.js") ;

var Region =  require("../region/Region.js") ;
var CleanUp = require("../utils/CleanUp.js") ;
var FileIO = require("../utils/FileIO.js") ;
var Util = require("../utils/Util.js") ;

/********************************************************************************
 Functions that invoke PS learn
 ********************************************************************************/
function invokePS(inRegion, GlobalDocument){
    var PSprog = []
    //Then we learn the position sequence, PS, which can either be LinesMapLearnPS or FilterIntLearn
    var progs1 = PSLearn.LinesMapLearnPS(inRegion, GlobalDocument )
    PSprog = PSprog.concat(progs1)
    if(!Util.isEmpty(progs1)) {
        Util.logLearn("PS-LinesMap:"+progs1.length)
    }else {
        Util.logLearn("PS-LinesMap:0")
    }
    //PS could either be of FilterIntLearnLS or LinesMapLearn
    var progs2 = PSLearn.FilterIntLearnPS(inRegion)
    PSprog = PSprog.concat(progs2)

    if(!Util.isEmpty(progs2)) {
        Util.logLearn("PS-FilterInt:"+progs2.length)
    }else {
        Util.logLearn("PS-FilterInt:0")
    }
    return PSprog
}
/********************************************************************************
 Functions that invoke LS learn
 ********************************************************************************/
function invokeLS(inRegion, GlobalDocument){
    var LSprog
    //Next, run FilterIntLearnLS(), which returns another program
    LSprog=(LSLearn.FilterIntLearnLS(inRegion, GlobalDocument ));
    if(!Util.isEmpty(LSprog)) {
        Util.logLearn("LS:"+LSprog.length)
    }else {
        Util.logLearn("LS:0")
    }
    return LSprog
}
/********************************************************************************
 Learning Function for Map operators: LinesMap, StartSeqMap, and EndSeqMap SS
 ********************************************************************************/
/* Takes in PS, a set of positions
 * Given the end point by x, EndSeqMapLearnSS learns the starting position given by PS
 */
function EndSeqMapLearnSS(SetRegion, GlobalDocument){
    var PS = invokePS(SetRegion, GlobalDocument)
    if(Util.isEmpty(PS)  ){
        return []
    }

    Util.logTime(9, "START: EndSeqMapLearnSS()")

    //Organize the PS by result:
    var organizedPS = CleanUp.organizeByResult(PS, function(prog){
        var results = []
        SetRegion.getSetRegions().forEach(function(aRegion){
            if (prog instanceof LinesMapPSProgram) {
                results = results.concat(prog.execute(aRegion.getRegionLines()))
            } else if (prog instanceof FilterIntProgram) {
                 results = results.concat(prog.execute(aRegion.getRegion()))
            }
        })
        return _.filter(results, null)

    })

    // PS defines the endpoint. And learn the starting point based off the previous execution
    var prefixes = []
    //We learn prefixes based off the PS result
    organizedPS.forEach(function (aPSresult) {
        prefixes = prefixes.concat(PairLearn.PairLearnPrefixPos(SetRegion.getRegionAt(0), aPSresult.result,aPSresult.listofprogs))
    })

    Util.logLearn("EndSeqMapLearnSS() prefixes length:" + prefixes.length)
    Util.logLearn("EndSeqMapLearnSS() organizedPS length:" + organizedPS.length)

    var crossproduct = []
    //Then for each result and execute prefixes[] on it
    for(var i=0; i<organizedPS.length; i++){
        var PSresult = organizedPS[i].result

        for(var j=0; j<prefixes.length; j++){
            var prefixProgram = prefixes[j]

            var currProgResult = _.compact(prefixProgram.PairPrefixPos( PSresult, prefixProgram))

            if(currProgResult!=null && !Util.isEmptySet(currProgResult)
                //For the second condition, we have to make sure that the results aren't empty
                && CleanUp.isConsistent(currProgResult, SetRegion.getAllPositiveExamples(), true)) {

                    crossproduct.push({ 'result': currProgResult,
                        'prefixProgramIndex': j, 'listofPrevProgramIndex': i
                     })
            }
        }
    }

    Util.logLearn("EndSeqMapLearnSS() crossproduct length:" + crossproduct.length)

    //Organize by result and clean up
    var organized = CleanUp.organizeByGivenResult(crossproduct)

    Util.logLearn("EndSeqMapLearnSS() organized length:" + organized.length)

    var cleaned =  CleanUp.CleanUpGeneric( organized, SetRegion.getAllPositiveExamples(), false )

    /* For now we keep the length to 1, because the number of prefix pair programs learned is ~1000
     * and the number of programs inside a previous program list is ~1000
     */
    var programs = []
    /* Grab the index of the prefix program and the index of the list of previous programs from cleaned[].
     * Form the new MapSSProgram by creating combos of the prefix programs with a previous program listed in the corresponding listofprogs[]
     */
    if(cleaned!=undefined && cleaned.length>0) {
        for (var i = 0; i < 1; i++) {//for(var i=0; i<cleaned.length; i++){ //Loop through all elements in cleaned[]
            var prefixProgIndex = cleaned[i].prefixProgramIndex
            var prefixProgram = prefixes[prefixProgIndex]   //Grab the actual program from prefixes[] with the index given by prefixProgIndex

            //Grab the index of the listofprogs[] from cleaned
            var listIndex = cleaned[i].listofPrevProgramIndex

            //Then grab the actual list from organizedPS
            var listofPrevProgs = organizedPS[listIndex].listofprogs

            //Loop through every program in that list:
            for (var j = 0; j < listofPrevProgs.length; j++) {
                var prevProgram = listofPrevProgs[j]

                //Create a new MapSSProgram and store it in programs[]
                programs.push(new MapSSProgram(prefixProgram, prevProgram, 'prefix'))
            }

        }
    }

    /*if(programs.length>0) {
        programs.forEach(function(aProg) {
            var res = SetRegion.run(aProg, false)
            Util.logLearn(aProg.debug)
            Util.logLearn(res)
        })
    }*/

    Util.logTime(9, "END: EndSeqMapLearnSS()")

    Util.logLearn("EndSeqMapLearnSS() time:" + Util.getTimeDifference(9))
    Util.logLearn("EndSeqMapLearnSS() length:" + programs.length)

    return programs
}
exports.EndSeqMapLearnSS = EndSeqMapLearnSS;


/* Takes in PS, a set of positions
 * Given the start point by x, StartSeqMapLearnSS learns the end position given by PS
 */
function StartSeqMapLearnSS(SetRegion, GlobalDocument){
    var programs = []

    var PS = invokePS(SetRegion, GlobalDocument)
    if(Util.isEmpty(PS)  ){
        return []
    }

    Util.logTime(10, "START: StartSeqMapLearnSS()")

    //Organize the PS by result:
    var organizedPS = CleanUp.organizeByResult(PS, function(prog){
        var results = []
        SetRegion.getSetRegions().forEach(function(aRegion){
            if (prog instanceof LinesMapPSProgram) {
                results = results.concat(prog.execute(aRegion.getRegionLines()))
            } else if (prog instanceof FilterIntProgram) {
                results = results.concat(prog.execute(aRegion.getRegion()))
            }
        })
        return _.filter(results, null)

    })

    /* PS defines the startpoint. And learn the end point based off the positive examples:
     *  Pair(startpoint by PS, endpoint by positive examples)
     */
    var suffix = []
    //We learn suffix based off the PS result
    organizedPS.forEach(function (aPSresult) {
        suffix = suffix.concat(PairLearn.PairLearnSuffixPos(SetRegion.getRegionAt(0), aPSresult.result,aPSresult.listofprogs))
    })

    Util.logLearn("StartSeqMapLearnSS() prefixes length:" + suffix.length)
    Util.logLearn("StartSeqMapLearnSS() organizedPS length:" + organizedPS.length)

    var crossproduct = []
    //Then for each result and execute suffix[] on it
    for(var i=0; i<organizedPS.length; i++){
        var PSresult = organizedPS[i].result

        for(var j=0; j<suffix.length; j++){
            var pairProgram = suffix[j]

            var currProgResult = _.compact(pairProgram.PairSuffixPos( PSresult, pairProgram))

            if(currProgResult!=null && !Util.isEmptySet(currProgResult)
                //For the second condition, we have to make sure that the results aren't empty
                && CleanUp.isConsistent(currProgResult, SetRegion.getAllPositiveExamples(), true)) {

                    crossproduct.push({ 'result': currProgResult,
                        'suffixProgramIndex': j, 'listofPrevProgramIndex': i })
            }
        }
    }

    Util.logLearn("StartSeqMapLearnSS() crossproduct length:" + crossproduct.length)

    //Organize by result and clean up
    var organized = CleanUp.organizeByGivenResult(crossproduct)

    Util.logLearn("StartSeqMapLearnSS() organized length:" + organized.length)

    var cleaned =  CleanUp.CleanUpGeneric( organized, SetRegion.getAllPositiveExamples(), false )

    /* For now we keep the length to 1, because the number of suffix pair programs learned is ~1000
     * and the number of programs inside a previous program list is ~1000
     * Grab the index of the suffix program and the index of the list of previous programs from cleaned[].
     * Form the new MapSSProgram by creating combos of the prefix programs with a previous program listed in the corresponding listofprogs[]
     */
    if(cleaned!=undefined && cleaned.length>0) {
        for (var i = 0; i < 1; i++) {//for(var i=0; i<cleaned.length; i++){     //Loop through all elements in cleaned[]
            var suffixProgIndex = cleaned[i].suffixProgramIndex
            var suffixProgram = suffix[suffixProgIndex] //Grab the actual program from suffix[] with the idex given by prefixProgIndex

            //Grab the index of the listofprogs[] from cleaned
            var listIndex = cleaned[i].listofPrevProgramIndex
            //Then grab the actual list from organizedPS
            var listofPrevProgs = organizedPS[listIndex].listofprogs

            //Loop through every program in that list:
            for (var j = 0; j < listofPrevProgs.length; j++) {
                var prevProgram = listofPrevProgs[j]

                //Create a new MapSSProgram and store it in programs[]
                programs.push(new MapSSProgram(suffixProgram, prevProgram, 'suffix'))
            }

        }
    }

    /*if(programs.length>0) {
        programs.forEach(function(aProg) {
            var res = SetRegion.run(aProg, false)
            Util.logLearn(aProg.debug)
            Util.logLearn(res)
        })
    }*/

    Util.logTime(10, "END: StartSeqMapLearnSS()")

    Util.logLearn("StartSeqMapLearnSS() time:" + Util.getTimeDifference(10))
    Util.logLearn("StartSeqMapLearnSS() length:" + programs.length)

    return programs

}
exports.StartSeqMapLearnSS = StartSeqMapLearnSS;


function LinesMapLearnSS(SetRegion, GlobalDocument){
    var programs = []
    //We learn LS
    var LS = invokeLS(SetRegion, GlobalDocument)
    if(Util.isEmpty(LS)){
        return []
    }

    Util.logTime(11, "START: LinesMapLearnSS()")

    //Organize LS by result
    var organizedLS = CleanUp.organizeByResult(LS, function(prog){
        var results =  SetRegion.run(prog, true)
        return results
    })
    if(organizedLS==undefined || organizedLS.length<=0){
        return []
    }

    /* Learn Pairs of Positions based on positive regions divided by lines
     * Learn based on LS
     * The Learn method takes ina type region, so create a type region with a line of result from LSresult
     */

    //We learn from an LS result
    var aLSresult = organizedLS[0].result

    //line is a positive example we want to learn linesMap from
    var line = SetRegion.getAllPositiveExamples()[0]

    //Find the region that contains the line from aLSresult[], and assign it to region
    var regionStr = _.find(aLSresult, function(aRegion){
        return aRegion.indexOf(line) != -1
    })

    var positiveIndices = [regionStr.indexOf(line)]
    var regionExample = new Region(regionStr, positiveIndices, [line])
    var pairProgs = PairLearn.PairLearnPos(regionExample)

    Util.logLearn("LinesMapLearnSS() pairProgs length:" + pairProgs.length)
    Util.logLearn("LinesMapLearnSS() organizedLS length:" + organizedLS.length)

    //Crossproduct LS and Pairs
    var crossproduct = []

    //Then for each result in LSresult, we let it be x, and execute the pair of positions on it
    for(var i=0; i<organizedLS.length; i++){
        var LSresult = organizedLS[i].result

        for(var j=0; j<pairProgs.length; j++){
            var aPairProgram = pairProgs[j]
            var currProgResult = _.compact(aPairProgram.PairPos(LSresult, aPairProgram))

            if(currProgResult!=null && !Util.isEmptySet(currProgResult)
                //For the second condition, we have to make sure that the results aren't empty
                && CleanUp.isConsistent(currProgResult, SetRegion.getAllPositiveExamples(), true)) {

                crossproduct.push({ 'result': currProgResult,
                    'pairProgramIndex': j, 'listofPrevProgramIndex': i})
            }
        }
    }

    Util.logLearn("LinesMapLearnSS() crossproduct length:" + crossproduct.length)

    /* Cleanup:
     * Organize by result and clean up
     */
    var organized = CleanUp.organizeByGivenResult(crossproduct)

    Util.logLearn("LinesMapLearnSS() organized length:" + organized.length)

    var cleaned =  CleanUp.CleanUpGeneric( organized, SetRegion.getAllPositiveExamples(), false )

    /* Grab the index of the pair program and the index of the list of previous programs from cleaned[].
     * Form the new MapSSProgram by creating combos of the prefix programs with a previous program listed in the corresponding listofprogs[]
     */
    if(cleaned!=undefined && cleaned.length>0) {
        for (var i = 0; i < 1; i++) {//for(var i=0; i<cleaned.length; i++){     //Loop through all elements in cleaned[]
            var pairProgramIndex = cleaned[i].pairProgramIndex
            var pairProgram = pairProgs[pairProgramIndex]   //Grab the actual program from pairProgs[] with the index given by pairProgramIndex

            //Grab the index of the listofprogs[] from cleaned
            var listIndex = cleaned[i].listofPrevProgramIndex
            //Then grab the actual list from organizedPS
            var listofPrevProgs = organizedLS[listIndex].listofprogs

            //Loop through every program in that list:
            for (var j = 0; j < listofPrevProgs.length; j++) {
                var prevProgram = listofPrevProgs[j]

                var candidateProgram = new MapSSProgram(pairProgram, prevProgram, 'pair')
                //Create a new MapSSProgram and store it in programs[]
                programs.push(candidateProgram)
            }

        }
    }

    /*if(programs.length>0) {
        programs.forEach(function(aProg) {
            var res = SetRegion.run(aProg, true)
            Util.logLearn(aProg.debug)
            Util.logLearn(res)
        })
    }*/

    Util.logTime(11, "END: LinesMapLearnSS()")

    Util.logLearn("LinesMapLearnSS() time:" + Util.getTimeDifference(11))
    Util.logLearn("LinesMapLearnSS() length:" + programs.length)

    return programs
}
exports.LinesMapLearnSS = LinesMapLearnSS;

