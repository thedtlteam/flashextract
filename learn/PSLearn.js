var _ = require('lodash');

var LSLearn = require('./LSLearn.js');

var Position = require("../position/Position.js") ;
var LinesMapPSProgram =  require("../program/LinesMapPSProgram.js") ;
var FilterIntProgram =  require("../program/FilterIntProgram.js") ;

var Region =  require("../region/Region.js") ;
var SetRegionObj =  require("../region/SetRegion.js") ;

var CleanUp = require("../utils/CleanUp.js") ;
var Util = require("../utils/Util.js") ;

/********************************************************************************
 Functions that invoke LS learn
 ********************************************************************************/
function invokeLS(SetRegion, GlobalDocument){
    var LSprog = []

    //Run FilterIntLearnLS(), which returns another program
    LSprog = LSprog.concat(LSLearn.FilterIntLearnLS(SetRegion, GlobalDocument ));
    if(!Util.isEmpty(LSprog)) {
        Util.logLearn("LS:" + LSprog.length)
    }else{
        Util.logLearn("LS:0")
    }
    return LSprog
}

/********************************************************************************
 Learning Function, for PS
 ********************************************************************************/
//Learns PosSeq for FilterInt
function FilterIntPositionLearnPS(SetRegion){
    Util.logTime(5, "START: FilterIntPositionLearnPS()")

    //Generate position regionParents for regions
    var positions = []

    var exampleRegion = SetRegion.getRegionAt(0)
    var regionParent = exampleRegion.getRegion()

     //Loop through all positives in exampleRegion
     for(var t=0; t<exampleRegion.getPositiveRegions().length; t++){
         //positiveExNumber is the positive example number
         var exampleNum = t

         //indexS is the location of region within regionParent
         var indexS = exampleRegion.getPositiveIndexOf(exampleNum);

         var positionsForThisRegion = Position.PosSeqLearn(regionParent, indexS)//.concat(Position.PosSeqLearn(regionParent, region, indexS+region.length))

         positions = positions.concat(positionsForThisRegion)
     }

     //Util.logLearn("positions[] length:" + positions.length)

     /* Filter the positions that don't match any of the positive regions, so that later on when we are trying to find the offset of the
      * first positive region, we got rid of those that will return -1 for indexOf
      */
     var filtered =[]
     for(var i=0; i<positions.length; i++) {

         var pos = positions[i]

         //Execution of Pos on all the lines in inRegion
         var Z = Position.executePos(pos, regionParent)
         if(_.find(exampleRegion.getPositiveRegions(), function(elem){ return Z.join("").indexOf(elem)!=-1}) != undefined) {

             //If at least one element from Z exists in SetRegion.getPositiveRegions()
             filtered.push(pos)

         }
     }

    //Util.logLearn("filtered[] length:" + filtered.length)

    var organized = CleanUp.organizeByResult(filtered, function(pos){

        var results = []
         SetRegion.getSetRegions().forEach(function(aRegion){
             var newRegionStr = aRegion.getRegion()
             var positives = Position.executePos(pos, aRegion.getRegion())
             var indices = _.map(Position.getPosK(pos, aRegion.getRegion()), function(aExecResult){return aExecResult[0]})

             //After execution of the position program for each region, recreate the region based off of that execution
             var newRegion = new Region(newRegionStr, indices, positives)
             results.push(newRegion)
         })

         //After execution of pos on each region, store the results in a new SetRegion
         var newSetRegion = (new SetRegionObj(results))
         return newSetRegion

     })

    Util.logTime(5, "END: FilterIntPositionLearnPS()")

    //Util.logLearn("organized[] length:" + organized.length)
    //Util.logLearn("FilterIntPositionLearnPS():" + Util.getTimeDifference(5))

    return organized
}

//Since learning takes a while, save the ones we learned already
var learnedIntPS = []
var limit = 20;

/* FilterIntLearnLS version for learning PS (position regionParent)
 * Returns an array of progs
 */
function FilterIntLearnPS(SetRegion ){
    var cleaned = []

    var foundIndex = _.findIndex(learnedIntPS, function(elem){ return Util.isSetRegionEqual(elem.SetRegion, SetRegion)})
    if(foundIndex!=-1){
        cleaned = learnedIntPS[foundIndex].listofintPS
    }else {
        var matchProgs = [];
        /* This is where the only difference from the above FilterIntLearnLS: this one learn position regionParents for regions(similar to PositionLearn)
         *  whereas the other one takes results from FilterBoolLearn
         *  positionSeq[] contains a list of unique the results and the positions that return those results:
         *  {'result', 'listofprogs'}
         */
        var positionSeq = FilterIntPositionLearnPS(SetRegion)
        Util.logLearn("PS-PositionSeq:" + positionSeq.length)

        Util.logTime(6, "START: FilterIntLearnPS()")

        //Loop through each program
        positionSeq.forEach(function (positions) {
            var iter = 0;
            var init = Number.POSITIVE_INFINITY;

            //We let Q be result of type SetRegion
            var Q = positions.result

            //m is the number of total Regions in Q
            var m = Q.getRegionCount()

            for (var j = 0; j < m; j++) {
                var thetaJ = Q.getRegionAt(j)

                //Zj is the result of the execution of prog at region j (theta j)
                var Zj = thetaJ

                var Yj = thetaJ.getPositiveIndices()

                var offset = Zj.getLineNumber(Yj[0]) //Getting the minimum offset
                init = _.min([init, offset])

                for (var i = 0; i <= Yj.length - 2; i++) {
                    //t is the difference between two contiguous regions in Z.
                    var t = Zj.getLineNumber(Yj[i + 1]) - Zj.getLineNumber(Yj[i])

                    if (iter == 0) {
                        iter = t;
                    } else {
                        iter = Util.GCD(iter, t);
                    }
                }
            }
            if (iter == 0)    iter = 1;
            if (init != Number.POSITIVE_INFINITY) {
                positions.listofprogs.forEach(function (program) {
                    matchProgs.push(new FilterIntProgram(init, iter, program));
                })
            }
        })

        //Util.logLearn("matchProgs[] length:" + matchProgs.length)

        var uniqueResults = CleanUp.organizeByResult(matchProgs, function (program) {
            var results = SetRegion.run(program, false)
            return results
        })

        //Util.logLearn("uniqueResults[] length:" + uniqueResults.length)

        cleaned = CleanUp.CleanUp(uniqueResults, SetRegion.getAllPositiveExamples(), false)


        //Testing out executions of programs in cleaned[]
        /*if (cleaned != undefined) {
            cleaned.forEach(function (filterIntProg) {
                var results = SetRegion.run(filterIntProg, false)
                Util.logLearn(results)
            })
        }*/

        //Add the learned list to learnedIntPS[]
        learnedIntPS.push({'listofintPS':cleaned, 'SetRegion':SetRegion})

        //Make sure learnedIntPS doesn't exceed limits, otherwise delete the first item in the list
        if(learnedIntPS.length>limit){
            learnedIntPS.splice(0, 1)
        }
        Util.logTime(6, "END: FilterIntLearnPS()")

        //Util.logLearn("cleaned[] length:" + cleaned.length)
        //Util.logLearn("FilterIntLearnPS():" + Util.getTimeDifference(6))
    }


    return cleaned

}
exports.FilterIntLearnPS = FilterIntLearnPS;

/********************************************************************************
 Learning Function for Map operators: LinesMap, for PS
 Learning the position, p, involves GeneratePosition() from FlashFill
 ********************************************************************************/

//Learning the positions for LinesMapLearnPS
function LinesMapPositionLearnPS(SetRegion, aLSresult){
    Util.logTime(7, "START: LinesMapPositionLearnPS()")

    /*  Learn scalar expression, F, which are the position functions
     *  Learn them based on previous executions (LS in this case)
     *  aLSresult is one of the results of LS
     */

    //line is a positive example we want to learn positions from, here we take the first example
    var line = SetRegion.getAllPositiveExamples()[0]

    //Find the element in aLSresult that contains the line, and assign it to region
    var region = _.find(aLSresult, function(aRegion){
        return aRegion.indexOf(line) != -1
    })

    //Define the boundaries of the line with respect to region. These indicate what positions to learn
    var kst = region.indexOf(line)
    var kend = kst + line.length

    //Generate positions
    var positions = [];

    for(var k = kst; k<=kend; k++) {
        positions = positions.concat(Position.PositionLearn(region, k))
    }

   //Util.logLearn("positions[] length:" + positions.length)

    //Clean up
    var organized = CleanUp.organizeByResult(positions, function(pos){
        return _.filter(Util.LinesMap(pos, aLSresult), null);
    })

    //Q is all the positives regions divided by lines
    var Q = SetRegion.getAllPositiveExamples()

    var filtered  =  CleanUp.CleanUp( organized, Q , true)

    //Util.logLearn("filtered[] length:" + filtered.length)

    positions = filtered

    //Limit the number of positions for now
    if(positions!=undefined && positions.length > 100){
        positions = positions.slice(0, 50).concat(
            positions.slice(positions.length - 51, positions.length))   //We take the first 50 and the last 50 learned positions
    }

    Util.logTime(7, "END: LinesMapPositionLearnPS()")

    //Util.logLearn("LinesMapPositionLearnPS():" + Util.getTimeDifference(7))

    return positions
}

//Since learning takes a while, save the ones we learned already
var learnedLinesMapPS = []

/* LinesMapLearnPS() Learning LinesMap
 * Returns a list of positions sequences
 * Takes in a list of programs
 */
function LinesMapLearnPS(SetRegion, GlobalDocument){
    var allPrograms = []

    var foundIndex = _.findIndex(learnedLinesMapPS, function(elem){
        return Util.isSetRegionEqual(elem.SetRegion, SetRegion)
            && Util.isDocumentEqual(elem.GlobalDocument, GlobalDocument)
    })
    if(foundIndex!=-1){
        allPrograms = learnedLinesMapPS[foundIndex].listoflinesmapPS
    }else {
        //Q is all the positives regions divided by lines
        var Q = SetRegion.getAllPositiveExamples()

        //The list of LS programs
        var LSprog = invokeLS(SetRegion, GlobalDocument)
        if (Util.isEmpty(LSprog)) {
            return []
        }

        Util.logTime(8, "START: LinesMapLearnPS()")

        //Organize the LSprog[] by their results
        var organizedLS = CleanUp.organizeByResult(LSprog, function (prog) {
            var results = SetRegion.run(prog, true)
            return results
        })

        //The positions[] learned
        var positions = LinesMapPositionLearnPS(SetRegion, organizedLS[0].result)
        if (Util.isEmpty(positions)) {
            Util.logTime(8, "END: LinesMapLearnPS()")
            return []
        }
        Util.logLearn("PS-Position:" + positions.length)

        /* Crossproduct on positions and the organizedLS results of LSprog[]
         * Crossproduct becomes a list of: {'result', 'posIndex'(based on positions[]), 'listofPrevProgramIndex'(based on organized[])}
         */
        var crossproductResult = []
        for (var i = 0; i < positions.length; i++) {
            var pos = positions[i];
            for (var j = 0; j < organizedLS.length; j++) {
                var result = organizedLS[j].result;
                var candResult = _.filter(Util.LinesMap(pos, result), null)
                crossproductResult.push({'result': candResult, 'posIndex': i, 'listofPrevProgramIndex': j})
            }
        }

        //Util.logLearn("crossproductResult[] length:" + crossproductResult.length)

        /*
         *  And gather the results of that, so we can do Cleaning up/subsumption on that
         *  Organize by result, so we get an array of {'result', 'listOfItems'}
         */
        crossproductResult = CleanUp.organizeByGivenResult(crossproductResult)

        //And then perform subsuming on that
        crossproductResult = CleanUp.CleanUpGeneric(crossproductResult, Q, false)

        //Util.logLearn("cleanup crossproductResult[] length:" + crossproductResult.length)

        /*
         * Grab the index of position and index of previous programs list from crossproductresult[]
         * Then, grab the positions from positions[] and previous programs from organized[],
         * and then gather them in allPrograms[]
         */
        for (var i = 0; i < crossproductResult.length; i++) {
            var posIndex = crossproductResult[i].posIndex
            var pos = positions[posIndex]

            //Grab the index of the listofprogs[] from crossproductResult[i]
            var prevProgListIndex = crossproductResult[i].listofPrevProgramIndex

            //Loop through all the programs under organizedLS[prevProgListIndex].listofprogs
            for (var j = 0; j < organizedLS[prevProgListIndex].length; j++) {
                var prevProgram = organizedLS[prevProgListIndex].listofprogs[j]
                var prog = new LinesMapPSProgram(pos, prevProgram)
                allPrograms.push(prog)
            }

        }

        //Util.logLearn("allPrograms[] length:" + allPrograms.length)

        //Testing out executions of programs in allPrograms[]
        /*allPrograms.forEach(function (linesMapProg) {
            var results = SetRegion.run(linesMapProg, true)

            Util.logLearn(results)
        })*/

        //Add the learned list to learnedIntPS[]
        learnedLinesMapPS.push({'listoflinesmapPS':allPrograms, 'SetRegion':SetRegion, 'GlobalDocument':GlobalDocument})

        //Make sure learnedIntPS doesn't exceed limits, otherwise delete the first item in the list
        if(learnedLinesMapPS.length>limit){
            learnedLinesMapPS.splice(0, 1)
        }

        Util.logTime(8, "END: LinesMapLearnPS()")

        //Util.logLearn("LinesMapLearnPS():" + Util.getTimeDifference(8))


    }

    return allPrograms
}
exports.LinesMapLearnPS = LinesMapLearnPS;
