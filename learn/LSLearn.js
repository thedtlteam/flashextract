var _ = require('lodash');

var BLSLearn = require('./BLSLearn.js');

var FilterIntProgram =  require("../program/FilterIntProgram.js") ;

var CleanUp = require("../utils/CleanUp.js") ;
var Util = require("../utils/Util.js") ;

/********************************************************************************
 Functions that invoke LS learn
 ********************************************************************************/
function invokeBLS(SetRegion, GlobalDocument){
    var BLSprog = []

    /* First learn the boolean predicate by calling learnPredicates(), in turns
     * calls FilterBoolLearn(). learnPredicates() returns programs
     */

    //We let Q be a set of all positive examples from SetRegion
    var Q = (SetRegion.getAllPositiveExamples())

    BLSprog=(BLSLearn.learnPredicates(SetRegion, Q, GlobalDocument) );

    if(!Util.isEmpty(BLSprog)) {
        Util.logLearn("BLS:" + BLSprog.length)
    }else {
        Util.logLearn("BLS:0")
    }
    return BLSprog
}
/********************************************************************************
 Learning Function, where LS executes FilterIntLearn()
 ********************************************************************************/
//Returns an array of progs
function FilterIntLearnLS(SetRegion, GlobalDocument){
    //Learn BLS
    var progsFromBool = invokeBLS(SetRegion, GlobalDocument)
    if(Util.isEmpty(progsFromBool)){
        return []
    }

    Util.logTime(2, "START: FilterIntLearnLS()")

    var matchProgs = [];

    //We let Q be SetRegion
    var Q = (SetRegion)

    //m is the number of total Regions in SetRegion
    var m = SetRegion.getRegionCount()

    /* Filter for programs with the most "strict" filtering logic that filters as few elements as possible
     * A strict program is one where init is the minimum offset of the first line from executing progsFromBool,
     * which returns a list of lines, Z (Equivalent to line 50 in Gulwani's paper)
     * Y is the sequence of positives from region at j
     * And iter is the GCD of all distances between the indices of any two contiguous elements of Y
     */

    //Loop through each program
    progsFromBool.forEach(function(prog){
        var iter = 0;
        var init = Number.POSITIVE_INFINITY;

        for(var j=0; j<m; j++) {
            var thetaJ = Q.getRegionAt(j)

            //Zj is the result of the execution of prog at region j (theta j)
            var Zj = prog.execute(thetaJ.getRegionLines())

            var Yj = thetaJ.getPositiveRegions()

            var offset = indexOf(Zj, Yj[0])//Zj.indexOf(Yj[0]); 	//Getting the minimum offset
            init = _.min([init, offset])

            for (var i = 0; i <= Yj.length - 2; i++) {
                //t is the difference between two contiguous regions in Z.
                var t = indexOf(Zj, Yj[i + 1]) - indexOf(Zj, Yj[i])//Zj.indexOf( Yj[i + 1]) - Zj.indexOf( Yj[i])//
                if (iter == 0) {
                    iter = t;
                } else {
                    iter = Util.GCD(iter, t);
                }
            }
        }
        if (iter == 0)    iter = 1;
        if(init!=Number.POSITIVE_INFINITY)
            matchProgs.push(new FilterIntProgram(init, iter, prog));
     })


    var uniqueResults = CleanUp.organizeByResult(matchProgs, function(prog){
        var results =  SetRegion.run(prog, true)
        return results
    })

    var cleaned = CleanUp.CleanUp(uniqueResults, Q.getAllPositiveExamples(), false)

    Util.logTime(2, "END: FilterIntLearnLS()")

    return cleaned
}


//index of Yo in Zj
function indexOf(Zj, Yji){
    if(Yji == undefined) return undefined

    //Divide Yji by newlines
    //And take only the first one
    Yji = Util.splitByNewline(Yji)[0]

    //Find which line Yji occurs in Zj
   for(var g=0; g<Zj.length; g++){
       if(Zj[g].indexOf(Yji)!=-1){
            return (g+1)
       }
   }

   return undefined

}

exports.FilterIntLearnLS = FilterIntLearnLS;
