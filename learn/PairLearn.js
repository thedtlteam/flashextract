var _ = require('lodash');

var Position = require("../position/Position.js") ;
var PairProgram = require("../program/PairProgram.js") ;
var Region =  require("../region/Region.js") ;

var CleanUp = require("../utils/CleanUp.js") ;
var Util = require("../utils/Util.js") ;

/********************************************************************************
 Learning Function for Map operator: PairLearn
 ********************************************************************************/
//PairLearnPos list of learned programs, because it takes 10 seconds ot learn it (See BLSLearn.js)
var learnedPairLearnPos = []
var limit = 20

//Limit on the number of total positions learned
var positionLimit = 8;

//Learns a pair of positions that define the positive regions
function PairLearnPos(inRegion){

    Util.logTime(3, "START: PairLearnPos()")

    var possibleEndPositions = []
    var possibleStartingPositions = []

    var foundIndex = _.findIndex(learnedPairLearnPos, function(elem){ return Util.isRegionEqual(inRegion, elem.inRegion)})
    if(foundIndex!=-1){

        possibleEndPositions = learnedPairLearnPos[foundIndex].possibleEndPositions
        possibleStartingPositions = learnedPairLearnPos[foundIndex].possibleStartingPositions

    }else{
        //line is a positive example we want to learn prefixes from
        var line = inRegion.getPositiveRegionsAt(0)
        var region = inRegion.getRegion()

        var kst = region.indexOf(line)
        var kend = kst + line.length

        Util.logTime(100, "START: PairLearnPos() PositionLearn")
        var possiblePositions = []
        for(var k = kst; k<kend; k++) {
            possiblePositions = possiblePositions.concat(Position.PositionLearn(region, k))
        }
        Util.logTime(100, "END: PairLearnPos() PositionLearn")

        Util.logTime(101, "START: PairLearnPos() Filter")
        //Filter
        possiblePositions.forEach(function(apos){
            var execPos = Position.executePos(apos, region)

            /* Check for prefixes
             * We make sure that execPos prefix is exactly the same as the positive example (line)
             */
            var findIndex = execPos.indexOf( line)

            if(findIndex==0){   //A possible valid starting position, so add it to possibleStartingPositions[]
                possibleStartingPositions.push(apos)
            }

            /* Check for suffix
             * We make sure that execPos suffix is exactly the same as the positive example (line)
             */
            var compareIndex = execPos.length-line.length
            if(findIndex==compareIndex && findIndex!=-1){
                possibleEndPositions.push(apos)
            }


        })
        Util.logTime(101, "END: PairLearnPos() Filter")

        //Limit the number of positions for now
        if(possibleStartingPositions.length > positionLimit/2){
            possibleStartingPositions = possibleStartingPositions.slice(0, positionLimit/2)
            //possibleStartingPositions = possibleStartingPositions.slice(0, positionLimit/4).concat(
            //    possibleStartingPositions.slice(possibleStartingPositions.length - positionLimit/4, possibleStartingPositions.length))
        }
        if(possibleEndPositions.length > positionLimit/2){
            possibleEndPositions = possibleEndPositions.slice(0, positionLimit/2)
            //possibleEndPositions = possibleEndPositions.slice(0, positionLimit/4).concat(
            //    possibleEndPositions.slice(possibleEndPositions.length - positionLimit/4, possibleEndPositions.length))
        }

        Util.logLearn("PairLearnPos() start pos filtered length:" + possibleStartingPositions.length)
        Util.logLearn("PairLearnPos() end pos filtered length:" + possibleEndPositions.length)

        //Add the learned programs to learnedPairLearnPos[]
        learnedPairLearnPos.push({'inRegion':inRegion, 'possibleEndPositions':possibleEndPositions
                                  ,  'possibleStartingPositions':possibleStartingPositions})

        //Make sure learnedPairLearnPos doesn't exceed limits, otherwise delete the first item in the list
        if(learnedPairLearnPos.length>limit){
            learnedPairLearnPos.splice(0, 1)
        }
    }

    var programs = []
    //If B or A is null set, then we return empty
    if(possibleStartingPositions.length==0 || possibleEndPositions.length==0) {
        Util.logTime(3, "END: PairLearnPos()")
        return []
    }else{
        //Otherwise crossproduct the two and add them to programs[]
        possibleStartingPositions.forEach(function(programA){
            possibleEndPositions.forEach(function(programB){
                var candidateProgram = new PairProgram(programA, programB, 'pair')
                var result = candidateProgram.execute(inRegion.getRegion())
                //Add it only if the execution of it is valid
                if(result) {
                    programs.push(candidateProgram)
                }
            })
        })

    }

    Util.logTime(3, "END: PairLearnPos()")

    Util.logLearn("PairLearnPos() time:" + Util.getTimeDifference(3))
    Util.logLearn("PairLearnPos() length:" + programs.length)

    return programs;
}
exports.PairLearnPos = PairLearnPos;

//The N2 non-terminal calls only a PairLearn for inRegion to learn a positive examples
function PairLearnRegionPos(inRegion){
    var programs = PairLearnPos(inRegion)
    //To test we can run programs on inRegion
    programs.forEach(function (aPair) {
        Util.logLearn(aPair.debug)
        Util.logLearn(aPair.execute(inRegion.getRegion()))
    })

    return programs
}
exports.PairLearnRegionPos = PairLearnRegionPos;

//PairLearnPrefixPos list of learned programs, because it takes 20 seconds ot learn it (See BLSLearn.js)
var learnedPairLearnPrefixPos = []
/* Pair learns position for a prefix of the positive regions, given the endpoint, x (given by PSresult)
 * PSprograms are the programs that return aPSresult
 * Returns all possible prefixes
 */
function PairLearnPrefixPos(inRegion, aPSresult, PSprograms) {
    Util.logTime(4, "START: PairLearnPrefixPos()")

    var possibleStartingPositions = []

    var foundIndex = _.findIndex(learnedPairLearnPrefixPos, function(elem){ return Util.isRegionEqual(inRegion, elem.inRegion)})
    if(foundIndex!=-1){

        possibleStartingPositions = learnedPairLearnPrefixPos[foundIndex].possibleStartingPositions

    }else {
        //line is a positive example we want to learn prefixes from
        var line = inRegion.getPositiveRegionsAt(0)

        //Find the region that contains the line from aPSresult[], and assign it to region
        var region = _.find(aPSresult, function (aRegion) {
            return aRegion.indexOf(line) != -1
        })

        if (region == undefined) {
            Util.logTime(4, "END: PairLearnPrefixPos()")
            return []
        }
        var kst = region.indexOf(line)
        var kend = kst + line.length

        for (var k = kst; k < kend; k++) {
            possibleStartingPositions = possibleStartingPositions.concat(Position.PositionLearn(region, k))
        }

        //Filter
        var filtered = []
        possibleStartingPositions.forEach(function (apos) {
            var execPos = Position.executePos(apos, region)
            //We make sure that execPos prefix is exactly the same as the positive example (line)
            var findIndex = execPos.indexOf(line)
            if (findIndex == 0) {
                filtered.push(apos)
            }


        })

        possibleStartingPositions = filtered

        //Limit the number of positions for now
        if (possibleStartingPositions.length > positionLimit) {
            possibleStartingPositions = possibleStartingPositions.slice(0, positionLimit/2).concat(
                possibleStartingPositions.slice(possibleStartingPositions.length - positionLimit/2, possibleStartingPositions.length))
        }

        Util.logLearn("PairLearnPrefixPos() filtered length:" + possibleStartingPositions.length)

        //Add the learned programs to learnedPairLearnPrefixPos[]
        learnedPairLearnPrefixPos.push({'inRegion':inRegion, 'possibleStartingPositions':possibleStartingPositions})

        //Make sure learnedPairLearnPrefixPos doesn't exceed limits, otherwise delete the first item in the list
        if(learnedPairLearnPrefixPos.length>limit){
            learnedPairLearnPrefixPos.splice(0, 1)
        }
    }

    var programs = []
    //If B or A is null set, then we return empty
    if (possibleStartingPositions.length == 0 || PSprograms.length == 0) {
        Util.logTime(4, "END: PairLearnPrefixPos()")
        return []
    }else {
        //Otherwise crossproduct the two and add them to programs[]
        PSprograms.forEach(function (programB) {
            possibleStartingPositions.forEach(function (programA) {
                programs.push(new PairProgram(programA, programB, 'prefix'))
            })
        })
    }

    Util.logTime(4, "END: PairLearnPrefixPos()")

    Util.logLearn("PairLearnPrefixPos() time:" + Util.getTimeDifference(4))
    Util.logLearn("PairLearnPrefixPos() length:" + programs.length)

    return programs;
 }
exports.PairLearnPrefixPos = PairLearnPrefixPos;

//learnedPairLearnSuffixPos list of learned programs, because it takes 20 seconds ot learn it (See BLSLearn.js)
var learnedPairLearnSuffixPos = []
/* Pair learns position for a suffix of the positive regions, given the startpoint, x (given by PSresult)
 * PSprograms are the programs that return aPSresult
 * Returns all possible suffix
 */
function PairLearnSuffixPos(inRegion, aPSresult, PSprograms) {
    Util.logTime(5, "START: PairLearnSuffixPos()")

    //Let A be the starting point that we already know (posPrograms)

     /* We let B be the suffix program
      * Q is all the positives regions divided by lines
      */
    var possibleEndPositions = []

    var foundIndex = _.findIndex(learnedPairLearnSuffixPos, function(elem){ return Util.isRegionEqual(inRegion, elem.inRegion)})
    if(foundIndex!=-1){

        possibleEndPositions = learnedPairLearnSuffixPos[foundIndex].possibleEndPositions

    }else {
        //line is a positive example we want to learn suffixes from
        var line = inRegion.getPositiveRegionsAt(0)

        //Find the region that contains the line from aPSresult[], and assign it to region
        var region = _.find(aPSresult, function (aRegion) {
            return aRegion.indexOf(line) != -1
        })
        if (region == undefined) {
            Util.logTime(5, "END: PairLearnSuffixPos()")
            return []
        }

        var kst = region.indexOf(line)
        var kend = kst + line.length

        for (var k = kst; k < kend; k++) {
            possibleEndPositions = possibleEndPositions.concat(Position.PositionLearn(region, k))
        }

        var filtered = []
        possibleEndPositions.forEach(function (apos) {
            var execPos = Position.executePos(apos, region)

            //We make sure that execPos suffix is exactly the same as the positive example (line)
            var compareIndex = execPos.length - line.length
            var findIndex = execPos.indexOf(line)
            if (findIndex == compareIndex && findIndex != -1) {
                filtered.push(apos)
            }


        })

        possibleEndPositions = filtered

        //Limit the number of positions for now
        if (possibleEndPositions.length > positionLimit/2) {
            possibleEndPositions = possibleEndPositions.slice(0, positionLimit/2).concat(
                possibleEndPositions.slice(possibleEndPositions.length - positionLimit/2, possibleEndPositions.length))
        }

        Util.logLearn("PairLearnSuffixPos() filtered length:" + possibleEndPositions.length)

        //Add the learned programs to learnedPairLearnSuffixPos[]
        learnedPairLearnSuffixPos.push({'inRegion':inRegion, 'possibleEndPositions':possibleEndPositions})

        //Make sure learnedPairLearnSuffixPos doesn't exceed limits, otherwise delete the first item in the list
        if(learnedPairLearnSuffixPos.length>limit){
            learnedPairLearnSuffixPos.splice(0, 1)
        }
    }
     var programs = []
     //If B or A is null set, then we return empty
     if(possibleEndPositions.length==0 || PSprograms.length==0) {
         Util.logTime(5, "END: PairLearnSuffixPos()")
         return []
     }else{
         //Otherwise crossproduct the two and add them to programs[]
         PSprograms.forEach(function(programA){
             possibleEndPositions.forEach(function(programB){
                 programs.push(new PairProgram(programA, programB, 'suffix'))
             })
         })

     }
     Util.logTime(5, "END: PairLearnSuffixPos()")

     Util.logLearn("PairLearnSuffixPos() time:" + Util.getTimeDifference(5))
     Util.logLearn("PairLearnSuffixPos() length:" + programs.length)

    return programs;
}
 exports.PairLearnSuffixPos = PairLearnSuffixPos;


