/********************************************************************************
 Predicates functions and Predicate Learning
 ********************************************************************************/
var _ = require('lodash');
var TokenSeq =  require("../regex/TokenSeq.js") ;
var FilterBoolProgram = require("../program/FilterBoolProgram.js") ;
var GlobalDocument = null

//Init() creates the permutations of the tokens with max of 3
var generator =  new require("../regex/RegexGenerator.js").RegexGenerator() ;
var regex = generator.init()

/* All functions that have names ending in Regex returns the regex for the function
 * indicated by the first part of the function's name i.e. StartsWith
 */
var StartsWith =  function (r, x){
    if(x==null) return false;
    //If line x starts with regex r return true
    var patt = new TokenSeq(['^'].concat(r.getTokenSeq()));
    return patt.getRegex().test(x)
}

var EndsWith =  function (r,x){
    if(x==null) return false;
    var patt =( new TokenSeq(r.getTokenSeq().concat(['$']))).getRegexGlobal();
    return patt.test(x)
}

var Contains =  function (r, k, x){
    if(x==null) return false;
    //If line x contains k occurrences of regex r, return true.
    var patt = (new TokenSeq(r.getTokenSeq())).getRegexGlobal();
    var matcharr = x.match(patt);
    if(matcharr==null) return false;
    var count = matcharr.length;
    if (count==k) return true; else return false;
}

var PredStartsWith =  function (r, x){
    /* According to the text:
     * "If the line preceding x, prevX, in the input text file ends with regex r"
     * However, Gulwani also specifies an Ends version of this function, so
     * shouldn't this refer the the starting of prevX instead?????
     * Grab prevX from Q
     */
    var prevX =  GlobalDocument.getPredAt(x)
    return StartsWith(r,prevX)
}

var PredEndsWith =  function (r,x){
    var prevX =  GlobalDocument.getPredAt(x)
    return EndsWith(r,prevX)
}

var PredContains =  function (r, k, x){
    var prevX = GlobalDocument.getPredAt(x)
    //If line prevX contains k occurrences of regex r, return true.
    return Contains(r, k, prevX)
}

var SuccStartsWith =  function (r, x ){
    var succX = GlobalDocument.getSuccAt(x)
    return StartsWith(r,succX)
}

var SuccEndsWith =  function (r, x){
    var succX = GlobalDocument.getSuccAt(x)
    return EndsWith(r,succX)
}

var SuccContains =  function (r, k, x){
    var succX = GlobalDocument.getSuccAt(x)
    return Contains(r, k, succX)
}


/* Input: x of type string
 * predicate learning with one +ve instance: returns a list of predicate functions.
 */
function PredicateLearn(globalDocument, x){
    //Set the GlobalDocument here
    GlobalDocument = globalDocument

    var listofpredicate = [];
    //Loop through all regex combinations
    for(var t=0; t<regex.length; t++){
        /* Go through all ten predicate functions
         * Take only the ones that return true
         */
        var r = regex[t]
        var matches = x.match(r.getRegexGlobal())

        if(matches!=null){
            var k = matches.length;
             if( Contains( r, k, x)){
                 listofpredicate.push(new FilterBoolProgram(_.partial(Contains, r,k), "function:Contains, r:"+ r.getRegexString() +", k:"+k))
            }
            if( PredContains( r, k, x)){
                 listofpredicate.push(new FilterBoolProgram(_.partial(PredContains, r,k), "function:PredContains, r:"+ r.getRegexString()+", k:"+k))
            }
            if( SuccContains( r, k, x)){
                 listofpredicate.push(new FilterBoolProgram(_.partial(SuccContains, r,k), "function:SuccContains, r:"+ r.getRegexString() +", k:"+k))
            }
        }

        if( StartsWith( r, x)){
             listofpredicate.push(new FilterBoolProgram(_.partial(StartsWith, r), "function:StartsWith, r:"+ r.getRegexString()))
        }
        if( EndsWith( r, x)){
             listofpredicate.push(new FilterBoolProgram(_.partial(EndsWith, r), "function:EndsWith, r:"+ r.getRegexString() ))
        }
        if( PredStartsWith( r, x)){
            listofpredicate.push(new FilterBoolProgram(_.partial(PredStartsWith, r), "function:PredStartsWith, r:"+ r.getRegexString()))
        }
        if( PredEndsWith( r, x)){
             listofpredicate.push(new FilterBoolProgram(_.partial(PredEndsWith, r), "function:PredEndsWith, r:"+ r.getRegexString() ))
        }
        if( SuccStartsWith( r, x)){
              listofpredicate.push(new FilterBoolProgram(_.partial(SuccStartsWith, r), "function:SuccStartsWith, r:"+ r.getRegexString()))
        }
        if( SuccEndsWith( r, x)){
             listofpredicate.push(new FilterBoolProgram(_.partial(SuccEndsWith, r), "function:SuccEndsWith, r:"+ r.getRegexString() ))
        }
    }

    //Return partially bound functions (bound with the learned regex) such that x (the string input) is the free variable.
    return listofpredicate;
}
exports.PredicateLearn = PredicateLearn;