/**
 * This is called by an independent child process, in order to speed up the learning process
 * This is a fork to learn a subset of positive examples
 */
/********************************************************************************
 Imports
 ********************************************************************************/
var _ = require('lodash');

var Region =  require("../region/Region.js") ;
var SetRegion =  require("../region/SetRegion.js") ;
var GlobalDocument =  require("../region/GlobalDocument.js") ;
var Util = require("../utils/Util.js") ;

var SSLearn = require('./SSLearn.js');

//The call to this function is done by N1Learn.js in learnX()
//So we take the arguments from the call and send it to the synthesizer
//The first one is the SetRegion, and the second one is document
var args =(process.argv[2]);
console.log((process.argv[2][0]))

//return SynthesizeSeqRegionProg(convertToSetRegion(args.SetRegion), convertToGlobalDocument(args.document))

/********************************************************************************
Convert literals to js objects
 ********************************************************************************/

//Function to turn the json literals back into js objects
//input: {a:"f"}
//output: {this.a:"f"}
function convertToSetRegion(inSetRegion){
    //For each Region create a region
    //Then create the SetRegion
}
function convertToGlobalDocument(indoc){
    return new GlobalDocument(indoc.document)
}

function SynthesizeSeqRegionProg(inSetRegion, indocument) {

    console.log(SetRegion)
    console.log(document)

    //Program that holds the final program, a set of functions and associated params, {func:predFunction, 'params':[]}
    var prog = [];

    Util.logTime(37, "START: SynthesizeSeqRegionProg")

    //console.log(SetRegion.toString())

    //Call the method that learns SS, which would spawn the learn for PS, LS, and BLS
    var SSLinesMap = SSLearn.LinesMapLearnSS(SetRegion, document)

    if(!Util.isEmpty(SSLinesMap)){
        prog = prog.concat(SSLinesMap)
        Util.logLearn("SS-LinesMap:"+SSLinesMap.length)
    }else{
        Util.logLearn("SS-LinesMap:0")
    }
    Util.logLearn("-----")

    //Call EndSeqMapLearnSS
    var SSEndSeqMap = SSLearn.EndSeqMapLearnSS(SetRegion, document)
    if(!Util.isEmpty(SSEndSeqMap)){
        prog = prog.concat(SSEndSeqMap)
        Util.logLearn("SS-EndSeqMap:" + SSEndSeqMap.length)
    }else {
        Util.logLearn("SS-EndSeqMap:0")
    }
    Util.logLearn("-----")

    //Call StartSeqMapLearnSS
    var SSStartSeqMap = SSLearn.StartSeqMapLearnSS(SetRegion, document)
    if(!Util.isEmpty(SSStartSeqMap)){
        prog = prog.concat(SSStartSeqMap)
        Util.logLearn("SS-StartSeqMap:" + SSStartSeqMap.length)
    }else {
        Util.logLearn("SS-StartSeqMap:0")
    }
    Util.logLearn("-----")

    Util.logTime(37, "END: SynthesizeSeqRegionProg")

    Util.logLearn("SynthesizeSeqRegionProg() time:" + Util.getTimeDifference(37))
    Util.logLearn("SynthesizeSeqRegionProg() length:" + prog.length)

    Util.logLearn("-----")

    return prog
}
