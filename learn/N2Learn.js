/**
 * Learns Pair
 */
/********************************************************************************
    Imports
 ********************************************************************************/
var Util = require("../utils/Util.js") ;

var PairLearn = require('./PairLearn.js');

/********************************************************************************
    SynthesizeRegionProg
 ********************************************************************************/
/* Given a SetRegion, learn programs only if the ancestor is a structure ancestor of the field we are extracting
 * i.e. there is no sequence in between the ancestor and the field, thus we only extract one thing  from the ancestor
 */
function SynthesizeRegionProg(SetRegion) {
    Util.logTime(35, "START: SynthesizeRegionProg")

    var programs = [];
    for(var i=0; i<SetRegion.getRegionCount(); i++){
        var aRegion = SetRegion.getRegionAt(i)
        //Only learn if there is only one positive example to learn from in aRegion
        if(aRegion.getPositiveRegionsCount()==1) {
            //Learn a pair of positions to extract the positive examples from inRegion
            var programs = programs.concat(PairLearn.PairLearnRegionPos(aRegion))
            Util.logLearn("N2-Pair:" + programs.length)
            Util.logLearn("-----")
        }
    }

    Util.logTime(35, "END: SynthesizeRegionProg")

    Util.logLearn("SynthesizeRegionProg() time:" + Util.getTimeDifference(35))
    Util.logLearn("SynthesizeRegionProg() length:" + programs.length)

    Util.logLearn("-----")

    return programs;

}
exports.SynthesizeRegionProg = SynthesizeRegionProg;
