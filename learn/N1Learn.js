/**
 * Learns Merge
 */
/********************************************************************************
 Imports
 ********************************************************************************/
var _ = require('lodash');

var Region =  require("../region/Region.js") ;
var SetRegion =  require("../region/SetRegion.js") ;
var Util = require("../utils/Util.js") ;
var CleanUp = require("../utils/CleanUp.js") ;
var Combinatorics = require('../utils/combinatorics.js').Combinatorics;

//var PairProgram =  require("../program/PairProgram.js") ;
var MapSSProgram =  require("../program/MapSSProgram.js") ;
var MergeProgram =  require("../program/MergeProgram.js") ;

var SSLearn = require('./SSLearn.js');

var jsStringEscape = require('js-string-escape')

//Limit the number of programs learned in Merge
var progLimit = 10
/********************************************************************************
 Learning Function for Merge operator: MergeLearn
 ********************************************************************************/
/* @document is a String of the whole document
 * @input is an Array of Regions
 */
function MergeLearn(document, input){

    Util.logTime(0, "START: MergeLearn()")

    /* We have organized it so that the positive regions are the ones we extract and inRegion is
     * the ancestor region to extract these positive region from
     */
    var Thetas = input

    /* All that parts in inRegion that we will form partitions on
     * This is the Y from Figure 6. Line 27
     */
    var positiveTotal = Thetas.getAllPositiveExamples()

    /* Generate subsets for each region in SetRegion
     * (We make subsets of theta1 and theta2)
     */
    var allSubsets = Thetas.generateSubsets()

    //Create subsets of allSubsets[], and we call this X[]
    var X = generateAllSubset(allSubsets)

    //Filter and generate programs
    var m = input.getRegionCount()
    var filtered = filterX(X, positiveTotal, m);


    //Learn programs for the filtered X's
    var learnedX = learnX(filtered, document)

    if(learnedX.length==0){
        //call n2
        Util.logLearn("Can't learn anything with N1; call n2")
        return [];
    }

    //And then generate combinations of positiveTotal.length of X
    var T = Util.getCombinations(learnedX, positiveTotal.length)

    //Operating on the N1 programs...
    //Filter for T's that do cover all examples
    var filteredT = filterT(T, positiveTotal, document)

    //CleanUp
    var organized = CleanUp.organizeByResult(filteredT, function(prog){
        var res = []
        input.getSetRegions().forEach(function(aRegion){
            res = res.concat(prog.execute(aRegion.getRegion()))

        })

        //REMOVE DUPLICATES due to the possibilities of programs overlapping
        var filtres = _.uniq(res)
        return filtres
    })

    var filteredOrganized  =  CleanUp.CleanUp( organized, positiveTotal , false)

    Util.logTime(0, "END: MergeLearn()")

   return filteredOrganized
   // return []
}
exports.MergeLearn = MergeLearn;

//Generates X, Figure 6 Line 26
function generateAllSubset(array) {
    var cmb, a;
    var result = []
    cmb = Combinatorics.power(array);
    cmb.forEach(function(a){
        if(a.length!=0) {
            var newSetRegion = new SetRegion(a)
            result.push(newSetRegion)
        }
    });
    return result
}

//Given the set T, filter for T's that do cover all examples
function filterT(X, positiveTotal, document){
    var filtered = X.splice(0)
    var filteredMerge = []

    for(var i=0; i< filtered.length; i++){
        var candidateT = filtered[i]
        //Get all the positives from candidateT
        var elementPositives = []
        candidateT.forEach(function(arrayOfRegions){
            var thisPositives = arrayOfRegions.getAllPositiveExamples()
            elementPositives = elementPositives.concat(thisPositives)
        })
		console.log(elementPositives)

        /* Check if Telem doesn't cover all examples
         * We take the intersection, and compare the length to the positiveTotal
         * It would be incorrect to simply use .isEqual, because then the elementPositivs with
         * different orderings but are still equal to positiveTotal would be removed
         */
        if( _.intersection(elementPositives, positiveTotal).length == positiveTotal.length
            && elementPositives.length==positiveTotal.length){
            //CandidateT[] contains an array of SetRegions, and each SetRegion contains a list of learned programs
            //So, crossproduct the lists of programs, and turn each element in that crossproduct into a Merge Program

            //First add the list programs to partialPrograms[]
            //(The programs are partial in that they are to be combined with another partial program)
            var partialPrograms = []
            for(var j=0; j<candidateT.length;j++){
                //setRegionProgs[] must be limited to the progLimit (based on execution result)
                var setRegionProgs = candidateT[j].getProgramsN1()

                //Organize the setRegionProgs[] by the execution result
                var organized = CleanUp.organizeByResult(setRegionProgs, function(prog){
                    var isDividedByLines = true;
                    if(((_.isEqual(prog.type, 'prefix') || _.isEqual(prog.type, 'suffix')) &&  prog instanceof MapSSProgram)
                        ){
                        isDividedByLines = false
                    }
                    var res =  candidateT[j].run(prog, isDividedByLines)
                    return res
                })

                //Take only a certian number of programs per unique result
                var filteredSetRegionProgs = []
                organized.forEach(function(organizedElem){
                    filteredSetRegionProgs=filteredSetRegionProgs.concat( organizedElem.listofprogs.slice(0, progLimit) )
                })
                partialPrograms.push(filteredSetRegionProgs)
            }

            var combined = partialPrograms[0]
            for(var j=1; j<partialPrograms.length; j++){
                combined  = Util.cart_prod(combined, partialPrograms[j])
            }

            //Then turn each program set within combined[] into a MergeProgram
            for(var j=0; j<combined.length; j++){
                filteredMerge.push(new MergeProgram(combined[j]))
            }
        }
    }

    return filteredMerge
}

//Learn programs for X
function learnX(inX, document){
    var X = inX.splice(0)
    var removeIndex = []

    //Testing running in parallel
    /*var Parallel = require('paralleljs');
    var p = new Parallel(X);
    p.map(SynthesizeSeqRegionProg).then(function(){
        console.log(arguments)
    })*/

    /*for(var i=0; i< X.length; i++) {
        var final = {}
        final[0] = X[i].obj()
        final [1] = document.obj()

        var exec = require('child_process').exec;
        var child = exec('node ./learn/N1LearnChild.js '+final+'');
        child.stdout.on('data', function (data) {
            console.log('stdout: ' + data);
        });
        child.stderr.on('data', function (data) {
            console.log('stdout: ' + data);
        });
        child.on('close', function (code) {
            console.log('closing code: ' + code);
        });
    }*/
    for(var i=0; i< X.length; i++) {
        var subset = X[i]
        var N1programs = SynthesizeSeqRegionProg(subset, document);
        //If it is empty set returned, then remove the subset too
        if (Util.isEmpty(N1programs)) {
            removeIndex.push(i)
        } else {
            //Associate the list of programs to the subset
            subset.setProgramsN1(N1programs)
        }
        Util.logLearn("-----")

    }

    /* Remove elements from X based on removeIndex
     * Sort descending (biggest to smallest)
     */
    removeIndex = removeIndex.sort(function(a, b){return b-a} );
    removeIndex.forEach(function(index){
        X.splice(index, 1)
    })

    return X
}

/* Given the set X, filter it based on Y, all the positive examples
 * @m is the number of regions in input (from MergeLearn()), Figure 6, Line 26
 */
function filterX(X, Y, m){
    var filtered = X.splice(0)
    var removeIndex = []

    //Learn programs for each subset
    for(var i=0; i< filtered.length; i++){
        var subset = filtered[i]
        /* Check if the set is minimal subset WRT Y
         * And the number of regions in subset must be equal to the number of regions in input (from MergeLearn()), Figure 6, Line 26
         * And make sure that subset doesn't contain the same region multiple times
         * And subset must contain at least one positive example
         * If subset doesn't meet all the above criteria, then it is removed
         */
        if(isMinimal(subset, Y) && m==subset.getRegionCount()
            && subset.isRegionsUnique() && subset.getAllPositiveExamples().length>0){
            //We do the learning of subset later in learnX()
        }else{
            removeIndex.push(i)
        }
    }


    /* Remove elements from X based on removeIndex
     * Sort descending (biggest to smallest)
     */
    removeIndex = removeIndex.sort(function(a, b){return b-a} );
    removeIndex.forEach(function(index){
        filtered.splice(index, 1)
    })

    return filtered
}

//Check if subset[] is a minimal partition of Y
function isMinimal(subset, Y) {

    var subsetArray
    subsetArray =subset.getAllPositiveExamples()

    var isMinimal = isMinimalPartition(subsetArray, Y)
    return isMinimal
}


//Check if set1 is a minimal subset of set2
function isMinimalPartition(set1, set2Original){
    var set2 = set2Original.slice(0)
    if(set2.length<set1.length) //If set1 is bigger, then there is no way set2 is its superset
        return false

    //Loop through set1(the smaller set)
    for(var i=0; i<set1.length; i++){
        //Find the elem1's match in set2, let's call this elem2
        var elem1 = set1[i]
        var find = _.findIndex(set2, function(set2Elem){
            return _.isEqual(elem1, set2Elem) ||  set2Elem.indexOf(elem1)!=-1
        })

        if(find!=-1  ){
            /* The elem1 is found in set2, remove it
             * Remove elem2 from set2 (because it has already been counted)
             */
            set2.splice(find, 1)
        }else if(find==-1){  //If there exists an element in set1 that doesn't exist in set2, then return with false
            return false
        }

    }
    //At this point all the checks are passed, return true
    return true
}


function SynthesizeSeqRegionProg(SetRegion, document) {

    //Program that holds the final program, a set of functions and associated params, {func:predFunction, 'params':[]}
    var prog = [];

    Util.logTime(37, "START: SynthesizeSeqRegionProg")

    //console.log(SetRegion.toString())

    //Call the method that learns SS, which would spawn the learn for PS, LS, and BLS
    var SSLinesMap = SSLearn.LinesMapLearnSS(SetRegion, document)

    if(!Util.isEmpty(SSLinesMap)){
        prog = prog.concat(SSLinesMap)
        Util.logLearn("SS-LinesMap:"+SSLinesMap.length)
    }else{
        Util.logLearn("SS-LinesMap:0")
    }
    Util.logLearn("-----")

    //Call EndSeqMapLearnSS
    var SSEndSeqMap = SSLearn.EndSeqMapLearnSS(SetRegion, document)
    if(!Util.isEmpty(SSEndSeqMap)){
        prog = prog.concat(SSEndSeqMap)
        Util.logLearn("SS-EndSeqMap:" + SSEndSeqMap.length)
    }else {
        Util.logLearn("SS-EndSeqMap:0")
    }
    Util.logLearn("-----")

    //Call StartSeqMapLearnSS
    var SSStartSeqMap = SSLearn.StartSeqMapLearnSS(SetRegion, document)
    if(!Util.isEmpty(SSStartSeqMap)){
        prog = prog.concat(SSStartSeqMap)
        Util.logLearn("SS-StartSeqMap:" + SSStartSeqMap.length)
    }else {
        Util.logLearn("SS-StartSeqMap:0")
    }
    Util.logLearn("-----")

    Util.logTime(37, "END: SynthesizeSeqRegionProg")

    Util.logLearn("SynthesizeSeqRegionProg() time:" + Util.getTimeDifference(37))
    Util.logLearn("SynthesizeSeqRegionProg() length:" + prog.length)

    Util.logLearn("-----")

    return prog
}

