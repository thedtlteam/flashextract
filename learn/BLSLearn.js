var _ = require('lodash');

var Predicates = require("./Predicates.js") ;

var CleanUp = require("../utils/CleanUp.js") ;
var Util = require("../utils/Util.js") ;

//Since learning predicates takes a while, save the ones we learned already
var learnedPredicates = []
var limit = 20; //Limit of the number of predicates learned in learnedPredicates[]

 /********************************************************************************
 Learning Function, where BLS executes FilterBoolLearn()
 ********************************************************************************/
/* This function learns all predicates, figures out which on is the right one
 * @Q is all the positive examples in an Array
 * @GlobalDocument is the document of type GlobalDocument
 * @SetRegion is the set of regions we want to learn from
 */
function learnPredicates(SetRegion, Q, GlobalDocument){
    Util.logTime(1, "START: FilterBoolLearn()")

    var programs = FilterBoolLearn(SetRegion, Q, GlobalDocument)

    Util.logTime(1, "END: FilterBoolLearn()")

    return  programs
}

exports.learnPredicates = learnPredicates;


/* FilterBoolLearn takes in Q, a list of positive examples
 * There is not need to go through the inputs; just the regex
 * There is predicate.learn, predicate.learn loops through the regexes and the predicate function.
 */
function FilterBoolLearn(SetRegion, Q, GlobalDocument){
    //Q ==> Q


    var aPositiveExample = Q[0]

    /* Predicate learning with one +ve instance: returns a list of predicate functions.
     * First find if the positive example is already learned i.e. in learnedpredicates[]
     * Otherwise, calle PredicateLearn()
     */

    var listofpredicate = []

    var foundIndex = _.findIndex(learnedPredicates, function(elem){ return _.isEqual(elem.positiveExample, aPositiveExample)})
    if(foundIndex!=-1){
        listofpredicate = learnedPredicates[foundIndex].listofpredicates
    }else{
        listofpredicate = Predicates.PredicateLearn(GlobalDocument, Q[0])

        //Add the learned list to learnedPredicates[]
        learnedPredicates.push({'positiveExample':aPositiveExample, 'listofpredicates':listofpredicate})

        //Make sure learnedPredicates doesn't exceed limits, otherwise delete the first item in the list
        if(learnedPredicates.length>limit){
            learnedPredicates.splice(0, 1)
        }
    }

    //Test for each predicate if the predicate is true for all of Q
    var newlist = []
    for(var i=0;i<listofpredicate.length; i++){
        var p = listofpredicate[i];
        var result = p.execute(Q)
        if( result.length == Q.length){
            newlist.push(p)
        }
    }
    var uniqueResults = CleanUp.organizeByResult(newlist, function(prog){
        var results =  SetRegion.run(prog, true)
        return results
    })
    var cleaned =  CleanUp.CleanUp(uniqueResults, Q, false)

    return cleaned;
}