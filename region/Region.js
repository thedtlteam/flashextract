/********************************************************************************
	Region class/module

********************************************************************************/
var _ = require('lodash');
var Util = require("../utils/Util.js") ;

//Define the Region
//@region is the whole region as a string
//@positiveRegions is the array of positive regions of type Region
//@positiveIndices is a list of indices in which the positive examples appear
function Region(region, positiveRegionIndices, positiveRegions,
                negativeRegionIndices, negativeRegions) {
    this.region = region;
    this.positiveRegions = positiveRegions;
    this.negativeRegions = negativeRegions;
    this.positiveRegionIndices = positiveRegionIndices
    this.negativeRegionIndices = negativeRegionIndices

    //regionLines is region divided by lines
    this.regionLines = Util.splitAndRetainByNewline(region)/*= region.split("\n");

    //Drop the last newline if it exists
    if(_.isEqual(this.regionLines[this.regionLines.length-1], '')) this.regionLines.splice(this.regionLines.length-1, 1);

    //Retain the newline in the end of each element in the region
    this.regionLines = _.map(this.regionLines, function(line){return line + '\n'})
*/

};
Region.prototype.constructor = Region;

Region.prototype.getRegion = function( ) {
    return this.region
}
//Returns an array of positive regions in string
Region.prototype.getPositiveRegions = function( ) {
    return this.positiveRegions
}
//Returns an array of negative regions in string
Region.prototype.getNegativeRegions = function( ) {
    return this.negativeRegions
}
//Returns the number of positive regions
Region.prototype.getPositiveRegionsCount = function( ) {
    return this.positiveRegions.length
}
//Returns an array of positive regions in string
Region.prototype.getPositiveIndices = function( ) {
    return this.positiveRegionIndices
}
//Returns an array of negative regions in string
Region.prototype.getNegativeIndices = function( ) {
    return this.negativeRegionIndices
}
//Returns the index'th positive regions in string
Region.prototype.getPositiveRegionsAt = function(index ) {
    return this.positiveRegions[index]
}

//Given index, return the positive index from positiveRegionIndices array
//So the return is the location of postive example number, index
Region.prototype.getPositiveIndexOf = function(index) {
    return this.positiveRegionIndices[index]
}

//Return the whole region divided by lines in an array
Region.prototype.getRegionLines = function( ) {
    return this.regionLines
}

//Returns the line number given the index WRT to this.region
Region.prototype.getLineNumber = function(indexInRegion){
    var count = 0
    for(var t=0;t<this.regionLines.length; t++){
        var stBoundary  = count
        var endBoundary = count + this.regionLines[t].length

        if(stBoundary<=indexInRegion && indexInRegion<endBoundary){
            return (t+1)
        }
        count = endBoundary
    }
    return undefined
}

Region.prototype.toString = function () {
    return "Region(" + this.region + ", " +
        this.positiveRegionIndices + ", " +
        this.positiveRegions + ", " +
        this.negativeRegionIndices + ", " +
        this.negativeRegions + ")";
};


module.exports = Region;
