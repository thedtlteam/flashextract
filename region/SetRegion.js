/********************************************************************************
  SetRegion Object: Defines a class for an Array of Regions
 ********************************************************************************/

var _ = require('lodash');
var Combinatorics = require('../utils/combinatorics.js').Combinatorics;

var Region =  require("./Region.js") ;

//Define the SetRegion
//@inArray is an Array of Regions
//@programs is an Array of programs that can be run on the Regions
function SetRegion(inArray, programsN1, programsN2) {
    this.arrayOfRegions = inArray;
    //this.programs = programs
    this.programsN1 = programsN1
    this.programsN2 = programsN2
};
SetRegion.prototype.constructor = SetRegion;

//Returns the number of Regions in the Set
SetRegion.prototype.getSetRegions = function(){
    return this.arrayOfRegions
}

//Returns the number of Regions in the Set
SetRegion.prototype.getRegionCount = function(){
    return this.arrayOfRegions.length
}

//Return the region at index
SetRegion.prototype.getRegionAt = function(index){
    return this.arrayOfRegions[index]
}

//Given a program, run it on the regions
//@isDividedByLines indicate if the regions must be divided by lines before the program runs on it
SetRegion.prototype.run = function(program, isDividedByLines){
    var results = []
    this.arrayOfRegions.forEach(function(region){
        if(isDividedByLines) {
            var regionResult = program.execute(region.getRegionLines())
            results = results.concat(regionResult)
        }else {
            results = results.concat(program.execute(region.getRegion()))
        }
    })
    return results
}

//Checks if the set contains multiple regions that are the same
SetRegion.prototype.isRegionsUnique = function(){
    var allRegions = _.map(this.arrayOfRegions, function(region){
        return region.getRegion()
    })

    return _.uniq(allRegions).length == allRegions.length
}

//Given an array of regions, return all the positive examples from every single region in one Array
SetRegion.prototype.getAllPositiveExamples = function(){
    var positives = _.reduce(this.arrayOfRegions, function(positives, pos) {
        return positives.concat(pos.getPositiveRegions());
    }, []);
    return positives

}

//Generate subsets for each region in SetRegion, and return it all in an Array
//Equivalent to Figure 6, Line 26
SetRegion.prototype.generateSubsets = function(){
    var allSubsets = []

    this.arrayOfRegions.forEach(function(theta){
        //Create all possible subsets of examples within theta
        var positiveExamples = theta.getPositiveRegions()

        allSubsets = allSubsets.concat(generateAllSubsetRegion(theta.getRegion(), positiveExamples))
    })
    return allSubsets
}

/*SetRegion.prototype.setPrograms = function(inPrograms) {
    this.programs = inPrograms
}

SetRegion.prototype.getPrograms = function(){
    return this.programs
}*/
SetRegion.prototype.setProgramsN1 = function(inPrograms) {
    this.programsN1 = inPrograms
}
SetRegion.prototype.setProgramsN2 = function(inPrograms) {
    this.programsN2 = inPrograms
}

SetRegion.prototype.getProgramsN1 = function(){
    return this.programsN1
}
SetRegion.prototype.getProgramsN2 = function(){
    return this.programsN2
}

function generateAllSubsetRegion(regionString , array) {
    var cmb, a;
    var result = []
    cmb = Combinatorics.power(array);
    cmb.forEach(function(a){ //console.log(a);
        // if(a.length!=0) {
        //Create a Region object from a
        var strRegion =regionString// a.join("")
        //Form index
        var indices = []
        a.forEach(function(posExample){
            indices.push(strRegion.indexOf(posExample))
        })
        var region = new Region(
            strRegion,   //string
            indices,//positive examples's index
            a//positive examples (which is the whole region
        )
        result.push(region)
        //}
    });
    return result//_.flatten(result)
}


SetRegion.prototype.toString = function () {
    var str = ""
    this.arrayOfRegions.forEach(function(region){
        str = str + region.toString()+", "
    })
    return "SetRegion(" + str + ")";
};

SetRegion.prototype.obj = function () {
    return {'SetRegion':this}
};

module.exports = SetRegion;
