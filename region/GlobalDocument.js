/********************************************************************************
    Global Document Object.
    Info about the current document is stored in here
 ********************************************************************************/

var _ = require('lodash');
var Region =  require("./Region.js") ;

//Define the GlobalDocument
//@document is the whole region as a string
function GlobalDocument(document) {
    this.document = document;

    //documentLines is region divided by lines
    this.documentLines =  document.split("\n");
    //Drop the last newline if it exists
    if(_.isEqual(this.documentLines[this.documentLines.length-1], '')) this.documentLines.splice(this.documentLines.length-1, 1);
    //Retain the newline in the end of each element in the region
    this.documentLines = _.map(this.documentLines, function(line){return line + '\n'})

};
GlobalDocument.prototype.constructor = GlobalDocument;

GlobalDocument.prototype.getDocument = function() {
    return this.document
}

GlobalDocument.prototype.getDocumentByLines = function() {
     return this.documentLines
}
//Returns the line previous to that line in the this.documentLines[]
GlobalDocument.prototype.getPredAt = function(x) {
    var pos =  getLineNumber(this.document, x);
    return this.documentLines[pos-1]
}
//Returns the line succeeding to that line in the this.documentLines[]
GlobalDocument.prototype.getSuccAt = function(x) {
    var pos =  getLineNumber(this.document, x);
    return this.documentLines[pos+1]
}
//Get the line number of region with respect to the document (String)
function getLineNumber(inDoc, region) {
    var lines  =  inDoc.split("\n");

    for(var  i=0; i<lines.length; i++){
        var line = lines[i]
        if(line.indexOf(region)!=-1){
            return i
        }
    }
    return -1
}


GlobalDocument.prototype.toString = function () {
    return "GlobalDocument(" + this.document + ", "+ ")";
};

GlobalDocument.prototype.obj = function () {
    return {'document':this.document}
};

module.exports = GlobalDocument;
