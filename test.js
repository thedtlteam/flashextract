/**
 * Created by Maeda on 2/1/2015.
 *
 * Sample of how to use this module
 */
var GlobalDocument =  require("./region/GlobalDocument.js") ;
var SetRegion =  require("./region/SetRegion.js") ;
var Region =  require("./region/Region.js") ;

var FE = require("./dsltext.js");

//For now the starting point is at test()
test()
function test(){
    //Send an array of Region objects
    var document =  new GlobalDocument("DATASET 200:\nproteins (01)\ncalories 20\nproteins (80)\n" +
    "DATASET 100:\nproteins (90)\ncalories 90\nproteins (600)\n"
    + "DATASET 400:\nproteins (102)\ncalories 432\nproteins (324)\n");

    var blueregion1 ="DATASET 200:\nproteins (01)\ncalories 20\nproteins (80)\n" +
        "DATASET 100:\nproteins (90)\ncalories 90\nproteins (600)\n"
        + "DATASET 400:\nproteins (102)\ncalories 432\nproteins (324)\n"

    //Here are the positive examples to extract from the blues:
    var pos1 ="proteins (600)\n"
    var pos2 ="proteins (102)\n"
    //var pos3 ="proteins (324)\n"

    //We form the array of regions to be sent to Merge.Learn
    //Algorithm 2, line 6: The regions in the array must contain positive examples, or else those regions that don't will not be learned upon
    var input = new SetRegion([ new Region(blueregion1,
        [blueregion1.indexOf(pos1), blueregion1.indexOf(pos2)],
        [pos1, pos2]) ])


    FE.run(FE.isCallN2Learn(input), input, document, function(result){
        var assert = require('chai').assert
            , expectedresult = [ 'proteins (600)\n', 'proteins (102)\n', 'proteins (324)\n' ];
        console.log(result)

        assert.deepEqual(result, expectedresult, "Passed");
    })//run(input, document, function(k){})

}
/* //For now the starting point is at test()
test()
function test(){
    //Send an array of Region objects
    var document =  new GlobalDocument("DATASET 200:\nproteins (01)\ncalories 20\nproteins (80)\n" +
     "DATASET 100:\nproteins (90)\ncalories 90\nproteins (600)\n"
     + "DATASET 400:\nproteins (102)\ncalories 432\nproteins (324)\n");

    //Blue region is the ancestor region we want to extract from
    //Note: Because this is an array/bunch of Regions, we will call SeqRegion.Learn
    /*var blueregion1 ="proteins ()\ncalories 20\nproteins ()\n" // "proteins (01)\ncalories 20\nproteins (80)\n"
     var blueregion2 ="proteins ()\ncalories 90\nproteins ()\n" // "proteins (90)\ncalories 90\nproteins (600)\n"
     var blueregion3 ="proteins ()\ncalories 432\nproteins ()\n" // "proteins (102)\ncalories 432\nproteins (324)\n"

     //Here are the positive examples to extract from the blues:
     var pos1 ="calories 20\nproteins ()"//"calories 20\nproteins (80)\n"//"calories "  //" 20"////
     var pos2 ="calories 90\nproteins ()"//"calories 90\nproteins (600)\n"//"calories " //" 90"////

     //Positive examples must be split by lines

     //We form the array of regions to be sent to Merge.Learn
     //Algorithm 2, line 6: The regions in the array must contain positive examples, or else those regions that don't will not be learned upon
     var input = new SetRegion([ new Region(blueregion1, [blueregion1.indexOf(pos1)], [pos1]),
     new Region(blueregion2, [blueregion2.indexOf(pos2)], [pos2]),
     new Region(blueregion3, [], [])
     ])*


    var blueregion1 ="DATASET 200:\nproteins (01)\ncalories 20\nproteins (80)\n" +
     "DATASET 100:\nproteins (90)\ncalories 90\nproteins (600)\n"
     + "DATASET 400:\nproteins (102)\ncalories 432\nproteins (324)\n"

     //Here are the positive examples to extract from the blues:
     var pos1 ="proteins (600)\n"
     var pos2 ="proteins (102)\n"
     //var pos3 ="proteins (324)\n"

     //We form the array of regions to be sent to Merge.Learn
     //Algorithm 2, line 6: The regions in the array must contain positive examples, or else those regions that don't will not be learned upon
     var input = new SetRegion([ new Region(blueregion1,
     [blueregion1.indexOf(pos1), blueregion1.indexOf(pos2)],
     [pos1, pos2]) ])


    /*var document =  new GlobalDocument("Aaa-Abb-Cbb-\nCcc-Tbb-lbb-\n")
    var blueregion1 ="Aaa-Abb-Cbb-\n"
    var blueregion2 ="Ccc-Tbb-lbb-\n"


    //Here are the positive examples to extract from the blues:
    var pos1 ="Aaa"
    var pos2 ="Abb"

    //We form the array of regions to be sent to Merge.Learn
    //Algorithm 2, line 6: The regions in the array must contain positive examples, or else those regions that don't will not be learned upon
    var input = new SetRegion([ new Region(blueregion1,
        [0, 4],
        [pos1, pos2]),
        new Region(blueregion2,
            [],
            [])
    ])*

    FE.run(FE.isCallN2Learn(input), input, document, function(k){})//run(input, document, function(k){})

}*/
