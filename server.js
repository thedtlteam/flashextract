/**
 * Created by Maeda on 2/1/2015.
 *
 * When running the web app, run this file from command prompt:
 * node server.js
 */
var debug = []

/********************************************************************************
 Imports
 ********************************************************************************/
var _ = require('lodash');

var Region =  require("./region/Region.js") ;
var GlobalDocument =  require("./region/GlobalDocument.js") ;
var FileIO = require("./utils/FileIO.js") ;
var Util = require("./utils/Util.js") ;
var Combinatorics = require('./utils/combinatorics.js').Combinatorics;

var SetRegion =  require("./region/SetRegion.js") ;
var FE = require("./dsltext.js");

runServer();

function runServer() {
    var http = require("http");
    var express = require('express'), //express framework
        app = express();

    app.set('port', process.env.PORT || 8000);
    var verbose = true;

    http.createServer(app).listen(app.get('port'), function () {
        Util.logLearn('Express server listening on port ' + app.get('port'));
    });

    //app should send index.html to the user, when there is a connection
    app.get('/', function (req, res) {
        Util.logLearn('trying to load '+ __dirname + '/UI/texture.html');
        res.sendfile('/UI/texture.html', { root: __dirname });
    });

    //app should send index.html to the user, when there is a connection

    app.get('/extract', function (req, res) {
        Util.logLearn("Summon the learning!")
        //Run the extraction code:
        try {

            var result = FE.formRegionAndLearn(req.query.document, req.query.ancestor, req.query.toExtract, function(result){
                res.send({"result": result})
            })

        }catch(err){
            Util.writeLogTimeToFile()
            Util.writeLogLearnedToFile()
            Util.logLearn(err)
            res.send({"err":err, "req":req.query})
        }
    });


    //This handler will listen for requests on /*, any file from the root of our server.
    app.get('/*', function (req, res, next) {

        //This is the current file they have requested
        var file = req.params[0];
        //For debugging, we can track what files are requested.
        if (verbose) Util.logLearn('\t :: Express :: file requested : /UI/' + file);
        //Send the requesting client the file.
        res.sendfile(__dirname + '/UI/' + file);

    });
}