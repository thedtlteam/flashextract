var _ = require('lodash');
var PosSeq =  require("../position/PosSeq.js") ;
var Position = require("../position/Position.js") ;

/********************************************************************************
    A FilterInt program
 ********************************************************************************/
/* prevProgram holds the object to the previous program (FilterBoolProgam or PosSeq(positions
 * where r1 and r2 match in x))
 */
function FilterIntProgram( init, iter, prevProgram) {
    this.init = init;
    this.iter = iter;
    this.prevProgram = prevProgram;
    this.debug = " FilterIntProgram(init:" + init + ", iter:" + iter +
                 ", prevProgram:" + prevProgram.debug+")"
 };
FilterIntProgram.prototype.constructor = FilterIntProgram;

//S is the set of lines to execute this on
FilterIntProgram.prototype.execute = function(S){
    if(this.prevProgram instanceof PosSeq){
        /* An array of prevPrograms (in this case, it would PosSeq, a sequence of positions, corresponding to a positive region)
         * We expect the input of S to be :inRegion.getRegion()
         */
        var posSeqExec = Position.executePos(this.prevProgram, S)
        return this.FilterInt(this.init, this.iter,posSeqExec)
    }else {
        /* For this case of prevProgram not as an array of program ...
         * We expect the input of S is to be:inRegion.getRegionLines()
         */
        return this.FilterInt(this.init, this.iter, this.prevProgram.execute(S))
    }
}
/****************************************************************************************************
 * Remember to CHANGE THE INPUTS TO EXECUTE!!!! DIFFERENT INPUTS ARE EXPECTED for different kinds of prevProgram
 * The FilterIntLS makes a distinction of having lines and PosSeq doesn't
 ****************************************************************************************************/

/* Only gathers the k's based on positions
 * Only applicable if the prevProgram is of type Position
 * This function is used in PairProgram.js for executing pair programs (for the PS's in SS)
 * @S is of type String
 * returns an object {"string" that the position applies to, "position"}
 */
FilterIntProgram.prototype.getPositions = function( S){
    if(this.prevProgram instanceof PosSeq && S!=undefined && this.prevProgram!=undefined) {
        var pos = Position.getPosK(this.prevProgram, S)
        if(pos!=null)
            return {"string":S, "position":pos}
    }
    return []

}

/**************************************************************
 Program Execution Function, where FilterInt(init, iter, lines) returns lines that conforms to init and iter
 Returns LS, which is a modified list of lines
 ********************************************************************************/
FilterIntProgram.prototype.FilterInt = function (init, iter, S){
    var newS = [];

    newS.push(S[init-1])
    for(var t=init-1+iter;t< S.length; t=t+iter){
        newS.push(S[t])
    }

    return newS;
}

module.exports = FilterIntProgram;
