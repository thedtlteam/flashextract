var Position = require("../position/Position.js") ;
var _ = require('lodash');
var Util = require("../utils/Util.js") ;

/********************************************************************************
 A LinesMapPS program
 ********************************************************************************/
 function LinesMapPSProgram( position, LS) {
     this.position = position;
     this.LS = LS;
     this.debug = "LinesMapPSProgram(pos:"+position.debug+
                  ", prevProgram:"+LS.debug+")"
 };
 LinesMapPSProgram.prototype.constructor = LinesMapPSProgram;

//S is of type array
 LinesMapPSProgram.prototype.execute = function( S){
     if(!_.isArray(S)){
         S = Util.splitAndRetainByNewline(S)
     }
    return this.LinesMap(this.position, this.LS.execute(S))
 }

//Only gathers the k's based on positions
LinesMapPSProgram.prototype.getPositionLinesMap = function (p, LS){
    var newLS = [];
    LS.forEach(function(x){
        if(x!=undefined && p!=undefined) {
            var positionsResult = Position.getPosK(p, x)
            if (positionsResult != null) {
                newLS.push({"string": x, "position": positionsResult})
            }
        }
    });
    return newLS;
}

/* Only gathers the k's based on positions
 * Only applicable if the prevProgram is of type Position
 * This function is used in PairProgram.js for executing pair programs (for the PS's in SS)
 * @S is of type Array
 * returns an array of {"string" that the position applies to, "position"}
 */
LinesMapPSProgram.prototype.getPositions = function( S){
    var LSresult = this.LS.execute(S)
    return this.getPositionLinesMap(this.position, LSresult)
}

 /********************************************************************************
  Program Execution Function, where LinesMap(p, LS) returns a list of pos(x, LS).
  LS is a List<T> which is result of executing FilterInt
  p is a position attribute, where it is either AbsPos(k) or RegPos(rr, k), and rr is a regex pair
  ********************************************************************************/
 LinesMapPSProgram.prototype.LinesMap = function (p, LS){
     return LinesMap(p, LS)
 }

function LinesMap(p, LS){
    var newLS = [];

    LS.forEach(function(x){
        newLS.push(Position.executePos(p, x))
    });
    return _.filter(newLS, null);
}
//module.exports.LinesMap = LinesMap;


//module.exports.LinesMapPSProgram = LinesMapPSProgram;


module.exports = LinesMapPSProgram;
