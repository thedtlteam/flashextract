/********************************************************************************
    A FilterBool program
 ********************************************************************************/
/* @predicate is the partial function
 * @debug contains info on the predicate
 */
function FilterBoolProgram( predicate, debug) {
    this.predicate = predicate;
    this.debug = " FilterBoolProgram("+debug+")";
};
FilterBoolProgram.prototype.constructor = FilterBoolProgram;

FilterBoolProgram.prototype.execute = function (S){
    return this.FilterBool(S, this.predicate)
}

/********************************************************************************
 Program Execution Function, where BLS runs the program returned by FilterBoolLearn()
 ********************************************************************************/
/* Given a set of x, S, and
 * partial predicate function (the only thing needed is the x the string in order to gain the predicate value)
 * Return all lines which satisfy the given regex
 */
FilterBoolProgram.prototype.FilterBool=function(S, partialPredFunc ){
    var match = []
    for(var j=0;j<S.length; j++){
        var p = partialPredFunc
        var x = S[j]
        if(p(x)){
            match.push(x)
        }
    }

    return match
}

module.exports = FilterBoolProgram;