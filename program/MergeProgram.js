var _ = require('lodash');

var MapSSProgram = require("../program/MapSSProgram.js") ;
var PairProgram = require("../program/PairProgram.js") ;

var Util = require("../utils/Util.js") ;

/********************************************************************************
 A Merge Program
 ********************************************************************************/

/* @arrayOfPrograms
 * @debug contains info on the predicate
 */
function MergeProgram( arrayOfPrograms) {
    this.arrayOfPrograms = arrayOfPrograms;

    var debug = ""
    this.arrayOfPrograms.forEach(function(prog){
        debug = debug +" " + prog.debug+", "
    })
    this.debug = " MergeProgram("+ debug+")";
};
MergeProgram.prototype.constructor = MergeProgram;

//Takes in a string:
MergeProgram.prototype.execute = function (string){
    var seperatedLine = Util.splitAndRetainByNewline(string)

    var results = []
    this.arrayOfPrograms.forEach(function(aProg) {
        if(aProg instanceof MapSSProgram){  //N1 program
            if(_.isEqual(aProg.getType(), 'pair')){
                //If the Map program requires (LS), then we must divide the string by newlines (for LS)
                results = results.concat(aProg.execute(seperatedLine))

            }else if(_.isEqual(aProg.getType(), 'prefix') || _.isEqual(aProg.getType(), 'suffix')){
                //Otherwise, the Map program requires (PS), so execute on the string without dividing by newlines
                results = results.concat(aProg.execute(string))

            }
        }else if(aProg instanceof PairProgram){  //N2 program
            //If it is a Pair program, then we send in a string
            results = results.concat(aProg.execute(string))
        }


    })

    //REMOVE DUPLICATES due to the possibilities of programs overlapping
    var filtres = _.uniq(results)
    return filtres

    //return results
}

module.exports = MergeProgram;