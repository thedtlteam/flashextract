/********************************************************************************
 A Pair program
 ********************************************************************************/
var _ = require('lodash');

var Position = require("../position/Position.js") ;
var Util = require("../utils/Util.js") ;


//A and B are of type positions that extract a position given a region
function PairProgram( A, B, type) {
    this.startingPt = A;
    this.endingPt = B;
    this.type = type
    this.debug = " Pair(Program A:"+ A.debug+", Program B:"+ B.debug+", type:"+this.type+")";
};
PairProgram.prototype.constructor = PairProgram;

/********************************************************************************
 Program Execution Function, Pair
 Given positions, stPt and enPt, and region in string format, return the extracted region defined by those positions
 ********************************************************************************/
PairProgram.prototype.execute = function(x){
    if(_.isEqual(this.type, 'pair')){   //Then the previous program is a pair of positions (LinesMapSS)
         return this.PairPos(x, this)
    }else if(_.isEqual(this.type, 'prefix')){
        return this.PairPrefixPos(x, this)
    }else if(_.isEqual(this.type, 'suffix')){
        return this.PairSuffixPos(x, this)
    }

    return null
}

PairProgram.prototype.getType = function (){
    return this.type
}

/* Execution for a Pair program learned in PairLearnPrefixPos()
 * @x is the string/array of strings to execute PairPrefixPos on
 * @prog is the pair program
 */
PairProgram.prototype.PairPrefixPos = function(  x, prog){
    var endPtProg = prog.endingPt
    //The endpoint can either be LS or PS:
    var resultEndpoint = Position.executePosPS(endPtProg, x)

    if(Util.isEmpty(resultEndpoint))
        return null

    var results = []

    for(var i=0; i<resultEndpoint.length; i++) {
        var elem = resultEndpoint[i]

        var string = elem.string
        if(string != undefined) {
            var endingPoint = _.max(elem.position)
            var startpoint = Position.getPosK(prog.startingPt, string)
            if (_.isArray(startpoint)) startpoint = _.min(startpoint)

            results.push(Pair(string, startpoint, endingPoint))
        }
    }

    return results
}

/* Execution for a Pair program learned in PairLearnSuffixPos()
 * @x is the string/array of strings to execute PairSuffixPos on
 * @pairSuffixProgram is the pair program
 */
PairProgram.prototype.PairSuffixPos = function(  x, prog){
    //The startpoint can either be LS or PS:
    var stPtProg = prog.startingPt
    var resultStpoint = Position.executePosPS(stPtProg, x)

    if(Util.isEmpty(resultStpoint))
        return null

    var results = []

    for(var  i=0; i<resultStpoint.length; i++) {
        var elem = resultStpoint[i]

        var string = elem.string
        if(string != undefined) {
            var startpoint = _.min(elem.position)
            var endingPoint = Position.getPosK(prog.endingPt, string)
            if (_.isArray(endingPoint)) endingPoint = _.max(endingPoint)

            results.push(Pair(string, startpoint, endingPoint))
        }
    }

    return results

}


/* Execution of a pair program on a String/Array x
 * @x is the string/Array to execute aPairProgram
 */
PairProgram.prototype.PairPos = function(x, aPairProgram){
    var stPtProg = aPairProgram.startingPt
    var endPtProg = aPairProgram.endingPt
    var results = []

    var endingPoint
    var startingPoint
    var substr = ""
   if(x instanceof  Array){
         x.forEach(function (line) {
            endingPoint = Position.getPosK(endPtProg, line )
            if(endingPoint instanceof  Array) endingPoint = _.max(endingPoint)

             startingPoint = Position.getPosK(stPtProg, line )
             if(startingPoint instanceof  Array) startingPoint = _.min(startingPoint)

             substr = Pair(line, startingPoint, endingPoint)
             results.push(substr)
        })

    }else{ //In this case it is pure string, then simple pass inRegion to Pair
        endingPoint = Position.getPosK(endPtProg, x )
        if(endingPoint instanceof  Array) endingPoint = _.max(endingPoint)

        startingPoint = Position.getPosK(stPtProg, x )
        if(startingPoint instanceof  Array) startingPoint = _.min(startingPoint)

        substr = Pair(x, startingPoint, endingPoint)
        results.push(substr)
    }

    return results
}


function Pair(region, stPt, enPt){
    return region.substring(stPt, enPt)
}
module.exports = PairProgram;
