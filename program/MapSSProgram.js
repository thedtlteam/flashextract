var _ = require('lodash');

var Position = require("../position/Position.js") ;

/********************************************************************************
 A MapSSProgram program, an SS program that is executed by wither LinesMApSS, StartSeqMap, EndSeqMap
 ********************************************************************************/
/* @pairProgram is the pair program in an SS program
 * @prevProgram is either LS or PS
 */
function MapSSProgram( pairProgram, prevProgram, type) {
    this.pairProgram = pairProgram;
    this.prevProgram = prevProgram;
    this.type = type
    this.debug = "MappSSProgram(pairProgram:"+pairProgram.debug+
                 ", prevProgram:"+prevProgram.debug+")"
};
MapSSProgram.prototype.constructor = MapSSProgram;

MapSSProgram.prototype.getType = function (){
    return this.type
}

//Takes as input an Array strings or a string, S
MapSSProgram.prototype.execute = function (S){
    if(_.isEqual(this.type, 'pair')){   //Then the previous program is a pair of positions (LinesMapSS)

        //Execute to get LS first
        var LS = this.prevProgram.execute(S)
        //Then run Pair on LS
        return this.pairProgram.execute(LS)

    }else if(_.isEqual(this.type, 'prefix') || _.isEqual(this.type, 'suffix')){

        //Execute PS first
        //var PS = Position.executePS(this.prevProgram, S)
        var PS = this.prevProgram.execute(S)

        //Then run Pair on PS
        var pairedPS = this.pairProgram.execute(PS)

        return pairedPS

    }
    return null
}

module.exports = MapSSProgram
