var debug = []

/********************************************************************************
	Imports
********************************************************************************/
var _ = require('lodash');

var Region =  require("./region/Region.js") ;
var GlobalDocument =  require("./region/GlobalDocument.js") ;
var FileIO = require("./utils/FileIO.js") ;
var Util = require("./utils/Util.js") ;
var Combinatorics = require('./utils/combinatorics.js').Combinatorics;

var N1Learn =  require("./learn/N1Learn.js") ;
var N2Learn =  require("./learn/N2Learn.js") ;
var SetRegion =  require("./region/SetRegion.js") ;

/********************************************************************************
 Running the Learning Functions
 ********************************************************************************/


//For now the starting point is at test()
/*test()
function test(){
    //Send an array of Region objects
    var document =  new GlobalDocument("DATASET 200:\nproteins (01)\ncalories 20\nproteins (80)\n" +
    "DATASET 100:\nproteins (90)\ncalories 90\nproteins (600)\n"
    + "DATASET 400:\nproteins (102)\ncalories 432\nproteins (324)\n");

    //Blue region is the ancestor region we want to extract from
    //Note: Because this is an array/bunch of Regions, we will call SeqRegion.Learn
    /*var blueregion1 ="proteins ()\ncalories 20\nproteins ()\n" // "proteins (01)\ncalories 20\nproteins (80)\n"
     var blueregion2 ="proteins ()\ncalories 90\nproteins ()\n" // "proteins (90)\ncalories 90\nproteins (600)\n"
     var blueregion3 ="proteins ()\ncalories 432\nproteins ()\n" // "proteins (102)\ncalories 432\nproteins (324)\n"

     //Here are the positive examples to extract from the blues:
     var pos1 ="calories 20\nproteins ()"//"calories 20\nproteins (80)\n"//"calories "  //" 20"////
     var pos2 ="calories 90\nproteins ()"//"calories 90\nproteins (600)\n"//"calories " //" 90"////

     //Positive examples must be split by lines

     //We form the array of regions to be sent to Merge.Learn
     //Algorithm 2, line 6: The regions in the array must contain positive examples, or else those regions that don't will not be learned upon
     var input = new SetRegion([ new Region(blueregion1, [blueregion1.indexOf(pos1)], [pos1]),
     new Region(blueregion2, [blueregion2.indexOf(pos2)], [pos2]),
     new Region(blueregion3, [], [])
     ])*


    var blueregion1 ="DATASET 200:\nproteins (01)\ncalories 20\nproteins (80)\n" +
        "DATASET 100:\nproteins (90)\ncalories 90\nproteins (600)\n"
        + "DATASET 400:\nproteins (102)\ncalories 432\nproteins (324)\n"

    //Here are the positive examples to extract from the blues:
    var pos1 ="proteins (600)\n"
    var pos2 ="proteins (102)\n"
    //var pos3 ="proteins (324)\n"

    //We form the array of regions to be sent to Merge.Learn
    //Algorithm 2, line 6: The regions in the array must contain positive examples, or else those regions that don't will not be learned upon
    var input = new SetRegion([ new Region(blueregion1,
        [blueregion1.indexOf(pos1), blueregion1.indexOf(pos2)],
        [pos1, pos2]) ])


    /*var document =  new GlobalDocument("Aaa-Abb-Cbb-\nCcc-Tbb-lbb-\n")
     var blueregion1 ="Aaa-Abb-Cbb-\n"
     var blueregion2 ="Ccc-Tbb-lbb-\n"


     //Here are the positive examples to extract from the blues:
     var pos1 ="Aaa"
     var pos2 ="Abb"

     //We form the array of regions to be sent to Merge.Learn
     //Algorithm 2, line 6: The regions in the array must contain positive examples, or else those regions that don't will not be learned upon
     var input = new SetRegion([ new Region(blueregion1,
     [0, 4],
     [pos1, pos2]),
     new Region(blueregion2,
     [],
     [])
     ])

    run(input, document, function(k){})

}
*/

//Call this function if data you have is not yet in Region object
//Otherwise call run() directly
//Forming the regions given from the interface:
//@ancestor is information from client on the area on which to form programs on
//@toExtract are the positiveExamples
function formRegionAndLearn(documentstr, ancestor, positiveExamples, callback){

    var document =  new GlobalDocument(documentstr)

    Util.logLearn(JSON.stringify(ancestor))
    Util.logLearn("------------------------------")
    Util.logLearn(JSON.stringify(positiveExamples))
    Util.logLearn("------------------------------")

    //Form Regions from the ancestor
    var regions = []
    //And for each region, find which positiv examples belong to them
    for(var i=0; i<ancestor.length; i++){
        var regionString = ancestor[i].text
        var regionRange = ancestor[i].ranges[0]

        var regPositiveExamples = []
        var regPositiveIndices = []
        //Add positive examples to the above arrays
        for(var j=0; j<positiveExamples.length; j++){
            var candidatePosRange = positiveExamples[j].ranges[0]

            if(Util.isInBetween(candidatePosRange[0], candidatePosRange[1], regionRange[0], regionRange[1])){
                regPositiveExamples.push(positiveExamples[j].text)
                regPositiveIndices.push((candidatePosRange[0]-regionRange[0]))
            }

        }

        regions.push(new Region(regionString, regPositiveIndices, regPositiveExamples))

    }

    var input = new SetRegion(regions)

    run(isCallN2Learn(input), input, document, callback)


}
exports.formRegionAndLearn = formRegionAndLearn;

//Use this function when you have the objects otherwise use the above function to form the objects from strings
//Be sure to call isCallN2Learn() and pass that result to the parameter isCallN2 to ensure a faster order of learning.
//Runs the learn
function run(isCallN2, input, document, callback){
    console.log("Learning Started!")
    var programs = []
    if(isCallN2){
        programs = N2Learn.SynthesizeRegionProg(input)
        console.log("Learning Ended!")

        Util.logLearn("Done Learning!")

        programs.forEach(function(prog){
            Util.logLearn(prog.debug)
            Util.logLearn( input.run(prog, false) )
        })

        callback(input.run(programs[0], false))

    }else {
        programs = N1Learn.MergeLearn(document, input)
        if(programs.length==0){ //Call N2 if nothing was learned
            run(true, input, document, callback);
        }
        console.log("Learning Ended!")

        Util.logLearn("Done Learning!")

        programs.forEach(function(prog){
            Util.logLearn(prog.debug)
            Util.logLearn( prog.execute(document.getDocument()) )

        })

        callback(programs[0].execute(document.getDocument()))
    }

    Util.writeLogTimeToFile()
    Util.writeLogLearnedToFile()

    return programs
}
exports.run = run;


/*Given a SetRegion, determine whether to call N2 Learn or N1 Learn
 * Given a SetRegion, N2Learn learn programs only if the ancestor is a structure ancestor of the field we are extracting
 * i.e. there is no sequence in between the ancestor and the field, thus we only extract one thing  from the ancestor
 */
function isCallN2Learn(SetRegion){

    for(var i=0; i<SetRegion.getRegionCount(); i++){
        var aRegion = SetRegion.getRegionAt(i)
        //Only learn if there is only one positive example to learn from in aRegion
        if(aRegion.getPositiveRegionsCount()==1) {
            Util.logLearn("Call N2! Learn Pair Programs")

            return true
        }
    }
    Util.logLearn("Call N1! Learn Merge Programs")

    return false
}
exports.isCallN2Learn = isCallN2Learn;

/********************************************************************************
 Server Setup
 ********************************************************************************/
/*setup()

function setup() {
    var http = require("http");
    express = require('express'), //express framework
        app = express();

    app.set('port', process.env.PORT || 8000);
    verbose = true;

    http.createServer(app).listen(app.get('port'), function () {
        Util.logLearn('Express server listening on port ' + app.get('port'));
    });

    //app should send index.html to the user, when there is a connection
    app.get('/', function (req, res) {
        Util.logLearn('trying to load '+ __dirname + '/UI/texture.html');
        res.sendfile('/UI/texture.html', { root: __dirname });
    });

    //app should send index.html to the user, when there is a connection

    app.get('/extract', function (req, res) {
        Util.logLearn("Summon the learning!")
        //Run the extraction code:
        try {
        /*Util.logLearn(req.query.document)
        Util.logLearn(req.query.ancestor)
        Util.logLearn(req.query.toExtract)*

        var result = formRegion(req.query.document, req.query.ancestor, req.query.toExtract, function(result){
            res.send({"result": result})
        })
        //var result = formRegion(req.query.document, req.query.ancestor, req.query.toExtract)
        //res.send({"result": result})
        }catch(err){
            Util.writeLogTimeToFile()
            Util.writeLogLearnedToFile()
            Util.logLearn(err)
            res.send({"err":err, "req":req.query})
         }
    });


    //This handler will listen for requests on /*, any file from the root of our server.
    app.get('/*', function (req, res, next) {

        //This is the current file they have requested
        var file = req.params[0];
        //For debugging, we can track what files are requested.
        if (verbose) Util.logLearn('\t :: Express :: file requested : /UI/' + file);
        //Send the requesting client the file.
        res.sendfile(__dirname + '/UI/' + file);

    });
}


/********************************************************************************
 Writing out debug files
 ********************************************************************************/
//FileIO.writeToFile("debug.txt",FileIO.toString(debug))//debug file
//CleanUp.printdebug();




