/********************************************************************************
 Regex position module described by two regex r1 and r2 and the kth match of r1 and r2
 ********************************************************************************/
var TokenSeq =  require("../regex/TokenSeq.js") ;
var getFirstR1TokenSeq = function(){
    return getFirstR1TokenSeqLocal(this.r1)
};

var getFirstR2TokenSeq  = function(){
    return getFirstR2TokenSeqLocal(this.r2)
};

// Define the RegPos constructor
function RegPos(k, r1, r2) {
    this.k = k;
    this.r1 = r1;	//This comes straight from PositionLearn. r1 is an array of TokenSeqArr (IParts)
    this.r2 = r2;
    this.debug = "k:"+k +", r1:["+getFirstR1TokenSeqLocal(r1).tokens.join()+"], r2:["+getFirstR2TokenSeqLocal(r2).tokens.join()+"]"

};
// Set the "constructor" property to refer to RegPos
RegPos.prototype.constructor = RegPos;

//Get only the first combination of r1
RegPos.prototype.getFirstR1TokenSeq = getFirstR1TokenSeq
RegPos.prototype.getFirstR2TokenSeq = getFirstR2TokenSeq
module.exports = RegPos;


 function getFirstR1TokenSeqLocal(r1){
    var r1tot = []
    for(var i=0; i<r1.length; i++ ){
        r1tot.push( r1[i].TokenSeqArr[0].tokens )
    }
    return new TokenSeq(r1tot);
};

function getFirstR2TokenSeqLocal(r2){
    var r2tot = []
    for(var i=0; i<r2.length; i++ ){
        r2tot.push(r2[i].TokenSeqArr[0].tokens )
    }
    return new TokenSeq(r2tot);
};

