/********************************************************************************
     Position Related Functions/Modules
 ********************************************************************************/
var _ = require('lodash');

var TokenSeq =  require("../regex/TokenSeq.js") ;
var AbsPos =  require("./AbsPos.js") ;
var RegPos =  require("./RegPos.js") ;
var PosSeq =  require("./PosSeq.js") ;

var Region =  require("../region/Region.js") ;
var Util = require("../utils/Util.js") ;

var generator =  new require("../regex/RegexGenerator.js").RegexGenerator() ;
    generator.init()

var limit = 100;
var posLimit = 25
/********************************************************************************
    Evaluation/Execution Position Functions/Modules; Returns the characters that the position refers to
 ********************************************************************************/
//Evaluating an Absolute position of x (String) to k
function getCharAtAbsPos(x, k1, k2){
    if(k1<0){ k1 = x.length + k1  + 1}
    if(k2<0){ k2 = x.length + k2  + 1}
    var result = x.substring(k1, k2);
    if(result.length == 1)
        return  result
    else
        return null
}

//Evaluating an RegPos of x (String) given r1, r2, kth match
var getCharAtRegPos = function (x, r1, r2, k){
    /* Returns the substring represented by r1 and r2 at the kth position
     * If such a combination cannot be found, we return null
     */
    var r12 = new TokenSeq(r1.getTokenSeq().concat( r2.getTokenSeq()));

    if((r1.getRegex()).test(x)&& (r2.getRegex()).test(x) && (r12.getRegex()).test(x)){

        //Find a k1 and k2 at k given r12
        var k1=null, k2=null;
        var r12G = r12.getRegexGlobal();
        var kPos = k[0], kNeg = k[1];
        var matches = x.match(r12G)
        var kTot = matches.length
        var ctr = 0;

        while (( found= r12G.exec(x) ) !== null) {	//Start saving all matches
            ctr++
            if(ctr==kPos && ctr ==( kTot+kNeg+1) ){	//And check for negative version of pos too
                //Calculate k2 and k1
                k1 = found.index;
                k2 =  found.index + found[0].length // k2 is k1 plus the length of the match
                return x.substring(k1, k2)
            }
        }
    }
    return null;

}

/* Function for executing position learned from PosSeq
 * It returns a list of regions matching the given r1, r2
 */
function getAllCharsAtPosSeq(region, r1, r2){
    /* Returns the substring represented by r1 and r2 at the kth position
     * If such a combination cannot be found, we return null
     */
    var positions = getKAtPosSeq(region, r1, r2);
    return _.map(positions, function(aPosition){return region.substring(_.min(aPosition), _.max(aPosition ))})

}
exports.getAllCharsAtPosSeq = getAllCharsAtPosSeq;

/* Generic call for Position evaluation
 * Execution of Pos given position p and string x
 * executePos() returns the chars
 */
function executePos(p, x){
    if(p instanceof RegPos){
        var result = getCharAtRegPos (x, p.getFirstR1TokenSeq(), p.getFirstR2TokenSeq(), p.k)
        return result
    } else if(p instanceof PosSeq){
        var result = getAllCharsAtPosSeq(x, p.getFirstR1TokenSeq(), p.getFirstR2TokenSeq())
        return result
    } else {
        var result = getCharAtAbsPos(x, p.k[0], p.k[1])
        return result
    }
}
exports.executePos = executePos;


/********************************************************************************
 Evaluation/Execution Position Functions/Modules, Returns only the integers referring to an actual position in x
 ********************************************************************************/
//Evaluating an RegPos of x (String) given r1, r2, kth match
var getKAtRegPos = function (x, r1, r2, k){
    /* Returns the substring represented by r1 and r2 at the kth position
     * If such a combination cannot be found, we return null
     */
    var r12 = new TokenSeq(r1.getTokenSeq().concat( r2.getTokenSeq()));

    if((r1.getRegex()).test(x)&& (r2.getRegex()).test(x) && (r12.getRegex()).test(x)){

        //Find a k1 and k2 at k given r12
        var k1=null, k2=null;
        var r12G = r12.getRegexGlobal();
        var kPos = k[0], kNeg = k[1];
        var matches = x.match(r12G)
        var kTot = matches.length
        var ctr = 0;

        while (( found= r12G.exec(x) ) !== null) {	//Start saving all matches

             ctr++
             if(ctr==kPos && ctr ==( kTot+kNeg+1) ){	//And check for negative version of pos too

                //Calculate k2 and k1
                k1 = found.index;
                k2 =  found.index + found[0].length // k2 is k1 plus the length of the match

                return [k1, k2]
            }
        }
    }
    return null;

}

/* Function for executing position learned from PosSeq
 * It returns a list of regions matching the given r1, r2
 * @region must be of type String
 */
function getKAtPosSeq(region, r1, r2){
    /* Returns the substring represented by r1 and r2 at the kth position
     * If such a combination cannot be found, we return null
     */
    var r12 = new TokenSeq(r1.getTokenSeq().concat( r2.getTokenSeq()));
    var r1G = r1.getRegexGlobal()
    var r2G = r2.getRegexGlobal()

    if((r1.getRegex()).test(region)&& (r2.getRegex()).test(region) && (r12.getRegex()).test(region)){
        /* Gather all the matches of r1 and the corresponding k (which would be the index a match was found at + the length of tht match) in x
         * Gather all the matches of r2 and the corresponding k (which would be the index a match was found at) in x
         * And take all those matches of r1 and r2 that have the same k
         */
        var matchesR1 = []
        while (( found = r1G.exec(region) ) !== null) {	//Start saving all matches

            //Calculate k
            var k = found.index + found[0].length // kr1 is k plus the length of the match(prefix)
            var k1 = found.index;
            var k2 =  found.index + found[0].length // k2 is k1 plus the length of the match

            matchesR1.push({'k1':k1,'k2':k2, 'k':k, 'found':found[0]})
        }

        var matchesR2 = []
        while (( found = r2G.exec(region) ) !== null) {	//Start saving all matches

            //Calculate k
            var k = found.index; //kr2 is simply the index it was found at (suffix)
            var k1 = found.index;
            var k2 =  found.index + found[0].length // k2 is k1 plus the length of the match

            matchesR2.push({'k1':k1,'k2':k2,'k':k, 'found':found[0]})
        }

        //Take all the k's that match:
        var matches = []

        for(var ctr1 = 0; ctr1<matchesR1.length; ctr1++){
            for(var ctr2 = 0; ctr2<matchesR2.length; ctr2++){
                if(matchesR1[ctr1].k == matchesR2[ctr2].k
                    && !isOverlap(matchesR1[ctr1].k1, matchesR2[ctr2].k2, matches)) {
                    matches.push([matchesR1[ctr1].k1, matchesR1[ctr1].k, matchesR2[ctr2].k2])
                }
            }
        }

        return matches
    }
    return null;
}

//Used in the above function to check if a given possible (k1test,k2test) pair overlaps other (k1,k2s) in the given matchesArr
function isOverlap(k1test, k2test, matchesArr){
    for(var g=0;g<matchesArr.length; g++){
        var k1 = matchesArr[g][0]
        var k2 = matchesArr[g][2]

        if(k1 < k2test && k1test < k2){
            return true
        }

    }
    return false
}

//Evaluating an Absolute position of x (String) to k
function getKAtAbsPos(x, k1, k2){
    if(k1<0){ k1 = x.length + k1  + 1}
    if(k2<0){ k2 = x.length + k2  + 1}

    var result = x.substring(k1, k2);
    if(result.length == 1)
        return  k1
    else
        return null
}

/* Generic call for Position evaluation
 * Execution of Pos given position p and string x
 * getPosK() returns an array  of integers indicating position
 */
function getPosK(p, x){
    if(p instanceof RegPos){
        var result = getKAtRegPos (x, p.getFirstR1TokenSeq(), p.getFirstR2TokenSeq(), p.k)
         return result
    }else if(p instanceof PosSeq){
        var result = getKAtPosSeq(x, p.getFirstR1TokenSeq(), p.getFirstR2TokenSeq())
        return result
    }else{   //Absolute position
        return getKAtAbsPos(x, p.k[0], p.k[1])
    }
}

exports.getPosK = getPosK;

/********************************************************************************
    Learning of a Sequence of Positions
 ********************************************************************************/
//Since learning takes a while, save the ones we learned already
var learnedPosSeq = []
/* regionParent is the region as a whole string
 * reg is a positive example from regionParent
 * indexS is the location of reg within region
 * We pick values of k to be the start and end of the positiveExample, reg, and check r1 and r2 against the whole region, regionParent
 */
function PosSeqLearn(regionParent, /*reg,*/ indexS){
    var result = []

    var k = indexS

    //Preprocessing...
    var indistPairs = getAllMatches(regionParent);
    if(indistPairs.length>0){	//This is to avoid for k that won't generate any since indistPair is of length zero. Such cases happen if the previous k contains a character of the same class with current k character

       //Collecting the representative Tokens out of the grouped tokens determined by indistinguishability and all the permutations from 1 to 3
        var comboReps = formReps(regionParent, indistPairs)

        var foundIndex = _.findIndex(learnedPosSeq, function(elem){ return _.isEqual(elem.regionParent, regionParent) && _.isEqual(elem.k, k)})
        if(foundIndex!=-1){
            result = learnedPosSeq[foundIndex].result
        }else {

            var allR1 = [];
            var allR2 = [];
            Util.logTime(54, "START: PosSeq Find suffix and prefix")

            for (var i = 0; i < comboReps.length; i++) {

                /* Instead of testing the candidate regex on reg
                 * we test on regionParent.
                 */
                var candidateRegex = comboReps[i];

                //Make sure that the regex even works on regionParent.
                if (candidateRegex.getRegex().test(regionParent)) {

                    //var allMatches = regionParent.match(candidateRegex.getRegexGlobal());
                    var m;
                    var re = candidateRegex.getRegexGlobal();

                    while ((m = re.exec(regionParent)) !== null) {

                        var testingpoint = m.index + m[0].length;
                        if (testingpoint == k) {
                            //Suffix found
                            allR1.push({'k1': m.index, 'r1': candidateRegex});
                        } else if (m.index == k) {
                            //Prefix found
                            allR2.push({'k2': (k + m[0].length), 'r2': candidateRegex});
                        }
                    }
                }
            }
            Util.logTime(54, "END: PosSeq Find suffix and prefix")
            //Util.logLearn("PosSeq Find suffix and prefix time:" + Util.getTimeDifference(54))

            //Util.logLearn("R1 length:" + allR1.length + " k:" + k + " string:" + regionParent)
            //Util.logLearn("R2 length:" + allR2.length + " k:" + k + " string:" + regionParent)

            //Limit the learning
            var allR1length = allR1.length
            var allR2length = allR2.length
            if ((allR1length * allR2length) > posLimit*posLimit) {
                Util.logLearn("Prefixes and suffixes EXCEED length! ")

                if (allR1length > posLimit) {
                    allR1length = posLimit
                }
                if (allR2length > posLimit) {
                    allR2length = posLimit
                }
            }

            Util.logTime(55, "START: PosSeq Form pairs ")
            for (var i = 0; i < allR1length; i++) {
                var r1 = allR1[i];

                for (var j = 0; j < allR2length; j++) {
                    var r2 = allR2[j];

                    var r12 = new TokenSeq(r1.r1.getTokenSeq().concat(r2.r2.getTokenSeq()));
                    var substr12 = regionParent.substring(r1.k1, r2.k2)

                    if ((r12.getRegex()).test(substr12)) {
                        var r12G = r12.getRegexGlobal();
                        // var matches = regionParent.match(r12G);
                        var c = -1
                        var cTot = 0

                        while (( found = r12G.exec(regionParent) ) !== null) {	//Start saving all matches

                            var matchSubstr = regionParent.substring(found.index).match(r12G)[0];
                            cTot++
                            if (found.index <= k && k <= matchSubstr.length + found.index) {
                                c = cTot;
                            }
                        }

                        if (c != -1) {
                            var r1arr = GenerateRegex(r1.r1, regionParent, indistPairs);
                            var r2arr = GenerateRegex(r2.r2, regionParent, indistPairs);
                            var newpos = new PosSeq(r1arr, r2arr)
                            result.push(newpos);
                        }
                    }
                }
            }
            Util.logTime(55, "END: PosSeq Form pairs ")
            //Util.logLearn("PosSeq Form pairs:"+Util.getTimeDifference(55))

            //Util.logLearn("result length:" + result.length + " k:" + k + " string:" + regionParent)

            //Add the learned list to learnedPosSeq[]
            learnedPosSeq.push({'k':k, 'regionParent':regionParent, 'result':result})

            //Make sure learnedPosSeq doesn't exceed limits, otherwise delete the first item in the list
            if(learnedPosSeq.length>limit){
                learnedPosSeq.splice(0, 1)
            }
        }

    }
    return result
}
exports.PosSeqLearn = PosSeqLearn;

//Since learning takes a while, save the ones we learned already
var learnedSuffixAndPrefix = []
/********************************************************************************
 PositionLearn from FlashFill. Used for Learning SS and PS
 ********************************************************************************/
function PositionLearn( s, k){

    Util.logTime(12, "START: PositionLearn()" )


    var result = []
    result.push( new AbsPos( [k, -(s.length-k)] ) );

    //Preprocessing...
    var indistPairs = getAllMatches( s);
    if(indistPairs.length>0){	//This is to avoid for k that won't generate any since indistPair is of length zero. Such cases happen if the previous k contains a character of the same class with current k character

        //Collecting the representative Tokens out of the grouped tokens determined by indistinguishability and all the permutations from 1 to 3
        var comboReps = formReps(s, indistPairs)

        var foundIndex = _.findIndex(learnedSuffixAndPrefix, function(elem){ return _.isEqual(elem.s, s) && _.isEqual(elem.k, k)})
        if(foundIndex!=-1){
            result = learnedSuffixAndPrefix[foundIndex].result
        }else {
            //Util.logLearn("comboReps length:"+comboReps.length+" string:"+s)

            Util.logTime(14, "START: Find suffix and prefix")
            var allR1 = [];
            var allR2 = [];
            for (var i = 0; i < comboReps.length; i++) {

                /* Instead of testing the candidate regex on substring of s (s[0:k-1] and s[k:s.length-1],
                 * we test on s.
                 */
                var candidateRegex = comboReps[i];

                //Make sure that the regex even works on s.
                if (candidateRegex.getRegex().test(s)) {

                    //var allMatches = s.match(candidateRegex.getRegexGlobal());
                    var m;
                    var re = candidateRegex.getRegexGlobal();

                    while ((m = re.exec(s)) !== null) {
                        var testingpoint = m.index + m[0].length;
                        if (testingpoint == k) {
                            //Suffix found
                            allR1.push({'k1': m.index, 'r1': candidateRegex});
                        } else if (m.index == k) {
                            //Prefix found
                            allR2.push({'k2': k + m[0].length, 'r2': candidateRegex});
                        }
                    }
                }
            }
            Util.logTime(14, "END: Find suffix and prefix ")

            //Util.logLearn("Find suffix and prefix time:"+Util.getTimeDifference(14))

            //Util.logLearn("R1 length:"+allR1.length+" k:"+k+" string:"+s)
            //Util.logLearn("R2 length:"+allR2.length+" k:"+k+" string:"+s)

            //Limit the learning
            var allR1length = allR1.length
            var allR2length = allR2.length
            if((allR1length*allR2length)>posLimit*posLimit){
                Util.logLearn("Prefixes and suffixes EXCEED length! ")

                if(allR1length>posLimit) {
                    allR1length = posLimit
                }
                if(allR2length>posLimit) {
                    allR2length = posLimit
                }
            }

            Util.logTime(15, "START: Form pairs ")
            for (var i = 0; i < allR1length; i++) {
                var r1 = allR1[i];
                for (var j = 0; j < allR2length; j++) {
                    var r2 = allR2[j];

                    var r12 = new TokenSeq(r1.r1.getTokenSeq().concat(r2.r2.getTokenSeq()));
                    var substr12 = s.substring(r1.k1, r2.k2)
                    if ((r12.getRegex()).test(substr12)) {

                        /* Find c,
                         * c is the index at which r12 matches s.substring(k1, k2) in s
                         * So we have many occurrences of r12 (derived from s[k1:k2]) in s
                         * So c is the match where s.substring(k1, k2) occurs
                         */
                        var cTot = 0;
                        var c = -1

                        /* Collect all matches and count total occurrences.
                         * If a match happens to equal substr12, then record the occurrence number into c
                         */
                        var matches = []
                        var r12G = r12.getRegexGlobal();
                        while (( found = r12G.exec(s) ) !== null) {	//Start saving all matches

                            var matchSubstr = s.substring(found.index).match(r12G)[0];
                            matches.push(matchSubstr);
                            cTot++;

                            if (matchSubstr == substr12) {
                                c = cTot;
                            }
                        }

                        if (c != -1) {

                            //GenerateRegex for r1 and r2
                            var r1arr = GenerateRegex(r1.r1, s, indistPairs);
                            var r2arr = GenerateRegex(r2.r2, s, indistPairs);

                            result.push(new RegPos([c, -(cTot - c + 1)], r1arr, r2arr));
                        }
                    }
                }
            }

            //Util.logLearn("result length:"+result.length+" k:"+k+" string:"+s)

            //Add the learned list to learnedSuffixAndPrefix[]
            learnedSuffixAndPrefix.push({'k':k, 's':s, 'result':result})

            //Make sure learnedSuffixAndPrefix doesn't exceed limits, otherwise delete the first item in the list
            if(learnedSuffixAndPrefix.length>limit){
                learnedSuffixAndPrefix.splice(0, 1)
            }
            Util.logTime(15, "END: Form pairs")

            //Util.logLearn("Form pairs time:"+Util.getTimeDifference(15))
        }
    }
    //Util.logLearn("learnedSuffixAndPrefix length:"+learnedSuffixAndPrefix.length)

    Util.logTime(12, "END: PositionLearn() ")

    return result
}
exports.PositionLearn = PositionLearn;

//Returns an array of regex
function GenerateRegex(r, s, indistPairs){
    var mapped = []//'TokenSeqArr':[all tokens that return that same MatchesArr result as r];

    //Loop through all tokens in r
    for(var t=0; t<r.getNumTok(); t++){

        //Get the array of tokens in which those that are indistinguishable from token
        var token = r.getTokenSeq()[t];

        //Find the token in the indistinguishable set
        var found = false;
        for(var ic = 0; ic<indistPairs.length; ic++){

            var group = indistPairs[ic];
            for(var tc = 0; tc<group.TokenSeqArr.length; tc++){

                if(group.TokenSeqArr[tc].getRegexString() == token ){
                    found = true;
                    mapped.push({'TokenSeqArr':group.TokenSeqArr})
                    break;
                }
            }

            if(found){
                break
            }
        }
    }
    return mapped;
}

//Since learning takes a while, save the ones we learned already
var learnedComboReps = []
//Collecting the representative Tokens out of the grouped tokens determined by indistinguishability and all the permutations from 1 to 3
//@s is used to identified if the reps has already been formed
function formReps(s, indistPairs){
    var comboReps;

    var foundIndex = _.findIndex(learnedComboReps, function(elem){ return _.isEqual(elem.s, s)})
    if(foundIndex!=-1){
        comboReps = learnedComboReps[foundIndex].comboReps
    }else {
        var reps = []
        Util.logTime(13, "START: Rep Tok ")
        for (var i = 0; i < indistPairs.length; i++) {
            reps.push((indistPairs[i].TokenSeqArr[0].getRegexString()))
        }
        Util.logTime(13, "END: Rep Tok ")

        //Util.logLearn("Rep Tok  time:"+Util.getTimeDifference(13))

        //Permutations of reps of 1,2,3
        comboReps = constructPermutations(reps)

        //Add the learned list to learnedComboReps[]
        learnedComboReps.push({'s':s, 'comboReps':comboReps})

        //Make sure learnedComboReps doesn't exceed limits, otherwise delete the first item in the list
        if(learnedComboReps.length>limit){
            learnedComboReps.splice(0, 1)
        }
    }
    return comboReps
}

//Since learning takes a while, save the ones we learned already
var learnedMatches = []

/* Take s, apply a token T1 and look at all the matches it returns M(T1) on s
 * Returns a list of {'MatchesArr':[], 'TokenSeqArr':[all tokens that return that same MatchesArr result]};
 */
function getAllMatches(s){

    var mapped = [] //{'MatchesArr':[], 'TokenSeqArr':[all tokens that return that same MatchesArr result]};

    var foundIndex = _.findIndex(learnedMatches, function(elem){ return _.isEqual(elem.s, s)})
    if(foundIndex!=-1){
        mapped = learnedMatches[foundIndex].mapped
    }else {
        Util.logTime(16, "START: getAllMatches()")

        //Loop through all tokens
        for (i = 0; i < generator.getAllTokensCount(); i++) {

            //Search all of mapped to see if the results of the candidate matche any of the match arrays in mapped
            var candidate = new TokenSeq([generator.getAllTokensAt(i)]);

            if ((s.match(candidate.getRegex()))	    //First check if there is a match
                && candidate.getRegexString() != "^"  //IF one of them is ^ or $, then there is not match anyways
                && candidate.getRegexString() != "$") {

                //If so then form the match array
                var candidateMatchArr = []
                var candidateG = candidate.getRegexGlobal();
                while (( found = candidateG.exec(s) ) !== null) {	//Start saving all matches
                    candidateMatchArr.push(s.substring(found.index).match(candidateG)[0]);
                }

                /* Then search mapped to see if the MatchArr matches any in the mapped ones
                 * If so, group that candidate Token with it's indistinguishable buddies
                 * Otherwise, if no matches appear, add a new element into mapped
                 */
                var found = false;
                for (j = 0; j < mapped.length; j++) {

                    if (_.isEqual(candidateMatchArr, mapped[j].MatchesArr)) {	//Indistinguishably check

                        //Group buddies!!
                        mapped[j].TokenSeqArr.push(candidate)
                        found = true;
                        break;
                    }
                }
                if (!found) {

                    mapped.push({'MatchesArr': candidateMatchArr, 'TokenSeqArr': [candidate/*.getJSON()*/]})

                }
            }
        }

        //Add the learned list to learnedMatches[]
        learnedMatches.push({'s':s, 'mapped':mapped})

        //Make sure learnedMatches doesn't exceed limits, otherwise delete the first item in the list
        if(learnedMatches.length>limit){
            learnedMatches.splice(0, 1)
        }

        Util.logTime(16, "END: getAllMatches()")
    }
    return mapped;
}


//arr is an array of strings and each element is a string that contains a regex
function constructPermutations(arr){
    Util.logTime(17, "START: Construct Permutations ")

    var tot = []
    var permutate = (function() {
        var results = [];
        function doPermute(input, output, used, size, level) {
            if (size == level) {
                var word = new TokenSeq(JSON.parse(JSON.stringify(output)));
                results.push( word);
                return;
            }

            level++;
            for (var i = 0; i < input.length; i++) {
                if (used[i] === true) {
                    continue;
                }
                used[i] = true;
                output.push(input[i]);
                doPermute(input, output, used, size, level);
                used[i] = false;
                output.pop();
            }
        }

        return {
            getPermutations: function(input, size) {
                var output = [];
                var used = new Array(input.length);
                doPermute(input, output, used, size, 0);
                return results;
            }
        }
    })();

    //Produce all combos of length 1 to 3 containing character classes
    var permutations1 = permutate.getPermutations(arr, 1);
    var permutations2 = permutate.getPermutations(arr, 2);
    var permutations3 = permutate.getPermutations(arr, 3);

    permutations1 = permutations1.concat(permutations2)
    permutations1 = permutations1.concat(permutations3)
    permutations1.forEach(function(elem) {
        try{
            elem.getRegex(); //This statement tests out the validity of the regex
            tot.push(elem)
        }catch(e){console.log("ARGH the combo doesnt work!!!"+elem) }
    });

    //Shave duplicates with lodash
    tot = _.uniq(tot);

    Util.logTime(17, "END: Construct Permutations ")

    //Util.logLearn("Construct Permutations time:"+Util.getTimeDifference(17))

    return tot;
}

/********************************************************************************
 Execution of a general PS program
 ********************************************************************************/

/* This function is used in Learning Pair/Executing  if it is within an SS function
 * Returns the positions (integers) of posProgram on x
 * x could be of type string, array
 */
function executePosPS(posProgram, x){

    var FilterIntProgram =  require("../program/FilterIntProgram.js") ;
    var LinesMapPSProgram =  require("../program/LinesMapPSProgram.js") ;

    //The posProgram can either be LS or PS:
     var results = null
    if (posProgram instanceof LinesMapPSProgram) {
        if(x instanceof Array){   //Then in this case, PSresult is a simple array, probably the result of a previous program execution on a region causing it to return an array of strings
            results = posProgram.getPositions(x)
        }else if(x instanceof  String){    //It is a pure string
            //If it is a String we seperate x into an Array by dividing it by newlines:
            results = posProgram.getPositions(Util.splitAndRetainByNewline(x))
        }
    } else if (posProgram instanceof FilterIntProgram) {
        //FilterIntProgram will always expect an inRegion of type String, never an Array
        //unless it is in the process of learning a Pair function
        if(x instanceof Array){   //Then in this case, PSresult is a simple array, probably the result of a previous program execution on a region causing it to return an array of strings
             results = _.map(x, function(xElem){
                 return posProgram.getPositions(xElem)
             })
        }else if(x instanceof String) {    //It is a pure string
             results = posProgram.getPositions(x)
        }
    }

    if(results == null || results.length==0)
        return null

    return results
}
exports.executePosPS = executePosPS;


