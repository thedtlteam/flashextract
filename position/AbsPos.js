/********************************************************************************
    Absolute position module described by an integer k
 ********************************************************************************/

// Define the AbsPos constructor
function AbsPos(k) {
    this.k = k;
    this.debug = "k:"+k
};
// Set the "constructor" property to refer to AbsPos
AbsPos.prototype.constructor = AbsPos;
module.exports = AbsPos;

