/********************************************************************************
 P, describing positions by two regex r1 and r2
 ********************************************************************************/
var TokenSeq =  require("../regex/TokenSeq.js") ;
var getFirstR1TokenSeq = function(){
    return getFirstR1TokenSeqLocal(this.r1)
};
var getFirstR2TokenSeq  = function(){
    return getFirstR2TokenSeqLocal(this.r2)
};

// Define the RegPos constructor
function PosSeq(r1, r2) {
    this.r1 = r1;	//This comes straight from PosSeqLearn. r1 is an array of TokenSeqArr (IParts)
    this.r2 = r2;
    this.debug = "PosSeq(r1:["+getFirstR1TokenSeqLocal(r1).tokens.join()+
                 "], r2:["+getFirstR2TokenSeqLocal(r2).tokens.join()+"])"
};
// Set the "constructor" property to refer to RegPos
PosSeq.prototype.constructor = PosSeq;

//Get only the first combination of r1
PosSeq.prototype.getFirstR1TokenSeq = getFirstR1TokenSeq
PosSeq.prototype.getFirstR2TokenSeq = getFirstR2TokenSeq

module.exports = PosSeq;

function getFirstR1TokenSeqLocal(r1){
    var r1tot = []
    for(var i=0; i<r1.length; i++ ){
        r1tot.push( r1[i].TokenSeqArr[0].tokens )
    }
    return new TokenSeq(r1tot);
};

function getFirstR2TokenSeqLocal(r2){
    var r2tot = []
    for(var i=0; i<r2.length; i++ ){
        r2tot.push(r2[i].TokenSeqArr[0].tokens )
    }
    return new TokenSeq(r2tot);
};

