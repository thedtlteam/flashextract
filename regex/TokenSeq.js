/********************************************************************************
	//Define the tokenSeq constructor
	//Holds an array of tokens
********************************************************************************/

/* Define the TokenSeq constructor
 * Holds an array of tokens
 */
function TokenSeq(tokens) {
    this.tokens = tokens;
};
TokenSeq.prototype.constructor = TokenSeq;
TokenSeq.prototype.getRegexString = function(){
    return this.tokens.join("");
};
TokenSeq.prototype.getRegex  = function(){
    return new RegExp(this.tokens.join(""));
};
TokenSeq.prototype.getRegexGlobal  = function(){
    return new RegExp(this.tokens.join(""), "g");
};
TokenSeq.prototype.getTokenSeq  = function(){
    return this.tokens;
};
TokenSeq.prototype.getNumTok  = function(){
    return this.tokens.length;
};
module.exports = TokenSeq;

