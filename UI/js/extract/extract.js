var allPairs;
//Counter to indicate the next set of parent-child to learn
var ctr = 0;

//Code for invoking the extraction code when the user clicks on the "Learn" button
function execute_learn(allRanges) {
    var document = $("#txt").text();
    //Form the Schema
    allRanges = _.groupBy(allRanges, function(elem) { return elem.color }); //allRanges is an object with colors as keys

    //This flag checks if the user has selected the whole document as a region.
    //If not, then add an entry in allColorsArr
    var isGlobalParentExist = false;

    //Convert the object to an array of objects with colors as keys, i.e. convert each key-value pair to its own object
    var allColorsArr = []
    _.forIn(allRanges, function(value, key){

        //Sort the values by where the range starts
        var sortedValues = value.sort( function(object1, object2) {
            var a = object1.ranges[0][0]
            var b = object2.ranges[0][0]
            return a-b
        });

        //Change the flag if the whole document is already selected as a region
        if(_.isEqual(sortedValues.text, document) ){
            isGlobalParentExist = true;
        }

        allColorsArr.push(sortedValues)
    })

    //Check the flag and add a new element as neccessary
    if(!isGlobalParentExist){
        allColorsArr.push([{
                                color:"",
                                ranges:[
                                    [0, document.length]
                                ],
                                text:document
                            }
        ])

    }

    allPairs = []

    //For each allRanges pair, check if they have a parent-child relationship, and store that in allPairs[]
    for(var i=0; i<allColorsArr.length; i++){
        for(var j=0; j<allColorsArr.length; j++){
            if(i!=j && isAncestor(allColorsArr[i], allColorsArr[j])){
                allPairs.push({"ancestor":allColorsArr[i], "child":allColorsArr[j], "learned":false})
            }
        }
    }

    ctr = 0;
    //Learn the pairs one by one synchronously
    getLearn(document, allPairs[ctr].ancestor, allPairs[ctr].child, next);
}

function next(document, data){
    allPairs[ctr].learned = true
    allPairs[ctr].program = data
    ctr++;
    if(ctr<allPairs.length) {
        getLearn(document, allPairs[ctr].ancestor, allPairs[ctr].child, next);
    }
}

//Contacts the server for programs
function getLearn(document, color1, color2, callback){
    //Form the parameters to be sent to the server

    console.log("Learning the following pair:")
    console.log(color1)
    console.log(color2)

    var array = { "document": document, "ancestor": color1, "toExtract": color2 };
    var parameters = jQuery.param(array);

    var newHighlights = []

    //Send get request to the server
    $.get("/extract" + "?" + parameters, function (data) {
        if(!data["err"]) {
            //alert(JSON.stringify(data["result"]));
            //console.log(data)

            data.result.forEach(function (resultText) {
                var h = {
                    color: color2[0].color,
                    text: resultText,
                    ranges: [
                        [document.indexOf(resultText), document.indexOf(resultText) + resultText.length]
                    ],
                    program: ""//resultElem.program
                };
                //Check if the text is already highlighted by user!
                if(!isHighlighted(h.ranges[0][0], h.ranges[0][1], color2)) {
                    console.log(h)
                    newHighlights.push(h)
                }

            })
            drawHighlight(newHighlights)
            callback(document, data)
        }else{
            //alert(data["err"])
            //callback(document, data)
            console.log(data["err"])
            console.log(data["req"])
        }

    });
}

//If (st1, end1) is in between (st2, end2), then return true
function isInBetween(st1, end1, st2, end2){
    if(st2<=st1 && st1<=end2 && st2<=end1 && end1<=end2){
        return true
    }
    return false
}

//Array 1 and array 2 contains ranges. Return true is array 1 is ancestor of array 2
function isAncestorArray(array1, array2){

    //If there exists a color2 range that is in between a color1, then color1 is ancestor of color2!
    for(var j=0; j<array2.length; j++){
        for(var i=0; i<array1.length; i++){
            var st2 = array2[j][0]
            var end2 = array2[j][1]
            var st1 = array1[i][0]
            var end1 = array1[i][1]

            if(isInBetween(st2, end2, st1, end1)){
                return true
            }
        }
    }
    return false
}

//Check if color1 is a valid ancestor of color2
function isAncestor(color1, color2){
    //color1 is an ancestor of color2, if there exists a range in color2 that is in between a range in color1
    //Search color2 for a range that is in between color1's ranges
    for(var t=0; t<color2.length; t++){
        for(var n=0; n<color1.length; n++){

            var color2Ranges = color2[t].ranges
            var color1Ranges = color1[n].ranges

            if(isAncestorArray(color1Ranges, color2Ranges)){
                return true
            }
        }
    }

    return false
}



/* Given two ranges, check if they are equal. If so, then return true.
 * This function is used to check if the user highlighted an example already.
 * It would be misleading to highlight the examples that already highlighted.
 *
 * @toHighlightSt/Ed are int positions of the text to highlight
 * @alreadyHighlighted is an array of length one where the zeroth element is
 *   the array containing all the highlights from the user after he/she
 *   clicked on the "Learn" button
 */
function isHighlighted(toHighlightSt, toHighlightEd, alreadyHighlighted){
    //Traverse alreadyHighlighted
    for(var t=0; t<alreadyHighlighted.length; t++){
        var cond = alreadyHighlighted[t].ranges

        if(cond[0][0]==toHighlightSt && cond[0][1]==toHighlightEd){
            return true
        }
    }
    return false
}

function drawHighlight(all_ranges){
    var layers = layerRanges(all_ranges);

    //For each layer add a highlight text area (this allows overlapping highlights)
    for(var l = 0; l < layers.length; l++){
        $("#txt").highlightTextarea({
            ranges: layers[l]
        });

        $("#txt").removeData();
    }


}


function layerRanges(R){
    //console.log(R);
    var layers = [];
    for(var i = 0; i < R.length; i++){
        var a = R[i].ranges[0];
        var added = false;
        for(var j = 0; j < layers.length; j++){
            var layer = layers[j];
            var possible = true;
            for(var k = 0; k < layer.length; k++){
                var b = layer[k].ranges[0];
                if(a[0] <= b[1] && b[0] <= a[1])
                    possible = false;
            }
            if(possible){
                added=true;
                layer.push(R[i]);
                break;
            }
        }
        if(!added){
            layers.push([R[i]])
        }
    }
    //console.log("Highlight:")
    //console.log(layers);
    return layers;
}



