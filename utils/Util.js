/********************************************************************************
 Misc Functions
 ********************************************************************************/
var _ = require('lodash');
var Region =  require("../region/Region.js") ;
var Combinatorics = require('./combinatorics.js').Combinatorics;
var Position = require("../position/Position.js") ;

var FileIO = require("./FileIO.js") ;
var DeleteKey = require('key-del')
var jsStringEscape = require('js-string-escape')

var isConsoleLog = true

//Checks if an array is empty
//So [[]] or [] is considered empty
function isEmpty(array){
    if (array==undefined || array == null || array.length <= 0 || _.compact(array) == null ||
        _.compact(array).length <= 0 || _.flatten(array) == null || _.flatten(array).length<=0) {

        return true
    }
    return false
}
exports.isEmpty = isEmpty;

function chkDuplicates(arr,justCheck){
    var len = arr.length, tmp = {}, arrtmp = arr.slice(), dupes = [];
    arrtmp.sort();
    while(len--){
        var val = arrtmp[len];
        if (/nul|nan|infini/i.test(String(val))){
            val = String(val);
        }
        if (tmp[JSON.stringify(val)]){
            if (justCheck) {return true;}
            dupes.push(val);
        }
        tmp[JSON.stringify(val)] = true;
    }
    return justCheck ? false : dupes.length ? dupes : null;
}
exports.chkDuplicates = chkDuplicates;

//Check if set1 subsumes set2, i.e. if set1 is a subset of set 2
function isSubsume(set1, set2) {

    var existenceCount =0
    //For each element in set1 search set2 for its existence
    for(var t=0; t<set1.length; t++){
        var set1Elem = set1[t]
        if(isSubstring(set1Elem, set2)){
            existenceCount++
        }
    }
    if(existenceCount == set1.length)
        return true
    else return false
 }
//Checks if candSubstr exists as a substring in arr
function isSubstring(candSubstr, arr){
     for(var j=0; j<arr.length; j++){
        var arrElem = arr[j]
         if(arrElem.indexOf(candSubstr)!=-1 ){	//And check for its existence
             return true
        }
    }

    return false;
}
exports.isSubsume = isSubsume;


function GCD(x, y) {
    while (y != 0) {
        var z = x % y;
        x = y;
        y = z;
    }
    return x;
}
exports.GCD = GCD;

//Given a string, return an array split by newline, and making sure that the last newline doesn't create an extra unneccessary line
function splitByNewline(string) {
   if(_.isEqual(string.charAt(string.length-1),'\n')) string = string.substring(0, string.length-1)
   return string.split("\n")
}
exports.splitByNewline = splitByNewline;

//Given a string, return an array split by newline, and making sure to retain the newline
function splitAndRetainByNewline(string) {
    var newString= string.split("\n");

    //Drop the last newline if it exists
    if(_.isEqual(newString[newString.length-1], '')) newString.splice(newString.length-1, 1);

    //Retain the newline in the end of each element in the region
    newString = _.map(newString, function(line){return line + '\n'})
    return newString

}
exports.splitAndRetainByNewline = splitAndRetainByNewline;


//Check if an array is an array of "" or empty sets
function isEmptySet(inArr) {
    //There might be a chance that the sets are nested arrays
    //So flatten it
    inArr = _.flatten(inArr)

    if(inArr ==null || inArr.length ==0 ){
        return true
    }
    //Then check if all the contents are of length zero
    for(var ctr=0; ctr<inArr.length; ctr++){
        if(inArr[ctr]!=null && inArr[ctr].length != 0){   //If an element does contain something, then we return false
            return false

        }
    }
    return true
}
exports.isEmptySet = isEmptySet;

function getCombinations(array, k) {
    var a
    var results = []
    var cmb = Combinatorics.combination(array, k);
    while(a = cmb.next()){
         results.push(a)
    }
    return results
}
exports.getCombinations = getCombinations;

function cart_prod(X, Y) {
    var out = [];
    for (var i in X) for (var j in Y){ out.push([X[i], Y[j]]); /*debug.push([[X[i], Y[j]]])*/}
    return out;
}
exports.cart_prod = cart_prod;

//If (st1, end1) is in between (st2, end2), then return true
function isInBetween(inst1, inend1, inst2, inend2){
    var st1 = parseInt(inst1),
        end1 = parseInt(inend1),
        st2 = parseInt(inst2),
        end2 = parseInt(inend2)

    //console.log("("+st1+", " +end1+") "+"("+ st2+","+ end2+")")
    if(st2<=st1 && st1<=end2 && st2<=end1 && end1<=end2){
        return true
    }
    return false
}
exports.isInBetween = isInBetween;

//Used in PSLearn when learning a LinesMAp operator when it is not yet instantiated
function LinesMap(p, LS){
    var newLS = [];

    LS.forEach(function(x){
        newLS.push(Position.executePos(p, x))
    });
    return _.filter(newLS, null);
}
exports.LinesMap = LinesMap;

/********************************************************************************
    Functions related to comparing two objects
 ********************************************************************************/
//Compare the two regions
function isRegionEqual(region1, region2){
   if(_.isEqual(region1.getRegion(),region2.getRegion())
            && _.isEqual(region1.getPositiveRegions(),region2.getPositiveRegions())
            && _.isEqual(region1.getPositiveIndices(),region2.getPositiveIndices())
            && _.isEqual(region1.getNegativeRegions(),region2.getNegativeRegions())
            && _.isEqual(region1.getNegativeIndices(),region2.getNegativeIndices())
    ){
        return true
    }

    return false
}
exports.isRegionEqual = isRegionEqual;

//Compare the two set regions
function isSetRegionEqual(setRegion1, setRegion2){
    if(setRegion1.getRegionCount()==setRegion2.getRegionCount()){
        for(var i=0; i<setRegion1.getRegionCount(); i++){
            if(!isRegionEqual(setRegion1.getRegionAt(i), setRegion2.getRegionAt(i))){
                return false
            }
        }

        return true
    }
    return false
}
exports.isSetRegionEqual = isSetRegionEqual;

//Compare the two documents
function isDocumentEqual(document1, document2){
    if(_.isEqual(document1.getDocument(), document2.getDocument())){
        return true
    }
    return false
}
exports.isDocumentEqual = isDocumentEqual;


/********************************************************************************
  Functions for logging run time. At some point writeLogTimeToFile()
  must be called to see the results in a file
 ********************************************************************************/
var loggedTimes = []
var isLogTime = true;

//@ID indicates the loggedTime belongs to a particular task and should be grouped
function logTime(ID, functionString){

    if(isLogTime) {
        var date = new Date();
        if(isConsoleLog) {
            console.log((date.getTime() / 1000) + "\t" + jsStringEscape(functionString))
        }
        loggedTimes.push({"ID":ID, "message":jsStringEscape(functionString), "time":(date.getTime() / 1000)})

    }
}
exports.logTime = logTime;

//Given the ID, take the difference of the last two logs to get the time difference
function getTimeDifference(ID){
    var count = 0;  //Flag to indicate which time appear before the other
    var difference = 0;

    for(var i=loggedTimes.length-1; i>=0; i--){
        if(loggedTimes[i].ID==ID && count<2){
            if(count == 0){ //Ending time
                difference = loggedTimes[i].time
            }else if(count==1){ //Starting time
                difference = difference - loggedTimes[i].time

            }
            count ++;
            if(count == 2){
                return difference
            }
        }
    }

}
exports.getTimeDifference = getTimeDifference;

function organizeByID(){
    var organized = []

    for(var i=0; i<loggedTimes.length; i++){
        var item = loggedTimes[i]
        var id = loggedTimes[i].ID

        //Find the id in organized
        var find = _.find(organized, function(elem){return _.isEqual(elem.ID,id)})

        if(find!=undefined){   //If the id is found in organized[], add the item to the appropriate place

            //Find the id in organized[]
            for(var j=0; j<organized.length; j++){
                if(_.isEqual(organized[j].ID, id)){
                    //delete item.id
                    item = DeleteKey(item, ['ID'])
                    organized[j].items.push(item)
                    break;
                }
            }

        }else{      //If the id could not be found in organized, then create a new item in organized[]
            item = DeleteKey(item, ['result'])
            organized.push({'ID': id, 'items':[item]})
        }
    }
    return organized
}
function formSummary(){
    var organized = organizeByID()
    var summary = ""

    for(var i=0; i<organized.length; i++){
        var items = organized[i].items
        var ID = organized[i].ID
        var message = items[0].message

        summary = summary +"ID: "+ID+" Message:"+ message + " "+(parseInt(items.length)/2)+" - Duration (seconds)"+"\n"
        for(var j=0; j<items.length-1; j=j+2){
            var time1 = items[j].time
            var time2 = items[j+1].time
            var duration = parseInt(time2 - time1)

            if(duration>0) {
                summary = summary + " " + duration + "\n"
            }
        }
    }
    FileIO.writeToFile("logtimessummary.txt",  (summary) )

}

function writeLogTimeToFile(){
    formSummary()
    FileIO.writeToFile("logtimes.txt", FileIO.toString(loggedTimes) )
}
exports.writeLogTimeToFile = writeLogTimeToFile;

/********************************************************************************
 Functions for logging run time
 ********************************************************************************/
var isLogLearn = true;
var loggedLearnedMessages = []
function logLearn(message){

    if(isLogLearn) {
        if(isConsoleLog) {
            console.log(jsStringEscape(message))
        }
        loggedLearnedMessages.push(jsStringEscape(message))

    }
}
exports.logLearn = logLearn;


function writeLogLearnedToFile(){
    FileIO.writeToFile("loglearn.txt", FileIO.toString(loggedLearnedMessages ))
}
exports.writeLogLearnedToFile = writeLogLearnedToFile;