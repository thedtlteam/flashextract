/********************************************************************************
 Cleanup Functions
 ********************************************************************************/
var _ = require('lodash');
var DeleteKey = require('key-del')
var Util = require("./Util.js") ;

/*
 * Generic CleanUp function
 * uniqueResults[] has the same format as the return of organizeByResult()
 * Before a set of programs is sent to the CleanUp function, the input must be sorted in the following fashion:
 * A set of unique results and each result is associated with a list of programs: {'result':result, 'listofprogs':[]}
 */
function CleanUp(uniqueResults, positiveExamples, isScalar){

    //Util.logLearn("Start: CleanUp() length:" + uniqueResults.length)

    Util.logTime(20, "START: CleanUp")

    if(Util.isEmpty(uniqueResults) || Util.isEmpty(positiveExamples)){
        return undefined
    }
    //Keeps a list of all the items to be removed in the uniqueResult[]
    var removeIndex = []

    for(var i=0; i<uniqueResults.length; i++) {
        var result = uniqueResults[i]
        /*
         * Check here for consistency and add to remove program and then continue if the condition is matched
         * Check if the given results are "Consistent" with the positiveExamples
         * For each positive examples check if it exists given a result from uniqueResults[]
         */
        if (!isConsistent(result.result, positiveExamples, isScalar)) {
            //Add the result's index to removeIndex[], because we want to remove this one
            removeIndex.push(i)
        }
    }

    //The actual removing is done here
    //Sort descending (biggest to smallest)
    removeIndex = removeIndex.sort(function(a, b){return b-a} );
    removeIndex.forEach(function(index){
        uniqueResults.splice(index, 1)
    })

    removeIndex = []
    var newUniqueResults = []
    for(var i=0; i<uniqueResults.length; i++){
        var include = true;
        var result = uniqueResults[i]

        //Check if the result is a subset of another program result, then don't include prog
        for(var k=0; k<uniqueResults.length; k++){
            var resultk = uniqueResults[k]
            if( Util.isSubsume(resultk.result, result.result )   &&
                (!Util.isSubsume(result.result , resultk.result ) || k<i) &&
                 _.find(removeIndex, k)==undefined //Make sure that it is not a previous program or the program that you are comparing against (k) isn't already removed
            ){
                /*
                 * If result is a subset of resultk...
                 * and make sure that resultk is not a subset of result
                 */
                removeIndex.push(i)
                include = false
                break;
            }
        }
        if(include) newUniqueResults.push(uniqueResults[i])
    }

    //Organize the programs that passed the above tests into an array, tot[]
    var plucked = _.pluck(newUniqueResults, 'listofprogs');
    var tot = _.reduce(plucked, function(tot, elem){
        return tot.concat(elem)
    })

    Util.logTime(20, "END: CleanUp")

    //Util.logLearn("CleanUp() time:" + Util.getTimeDifference(20))
    //Util.logLearn("End: CleanUp() length:" + tot.length)


    return tot
}
exports.CleanUp = CleanUp;

/* Same as above, except the uniqueResults[] input is not expected to be as the above format,
 * except it must have the key values: 'result' and 'listOfItems'
 * The reason for having two CleanUp(), is due to the organization of the data passed into the functions
 * Some require a certain organization due to the large amounts of programs learned, thus forcing the data
 * to be organized by those program's results in order to reduce learning time
 */
function CleanUpGeneric(uniqueResults, positiveExamples, isScalar){

    //Util.logLearn("Start: CleanUp() length:" + uniqueResults.length)

    Util.logTime(21, "START: CleanUp")

    if(Util.isEmpty(uniqueResults) || Util.isEmpty(positiveExamples)){
        return undefined
    }

    var removeIndex = []    //Keeps a list of all the items to be removed in the uniqueResult[]

    for(var i=0; i<uniqueResults.length; i++) {
        var result = uniqueResults[i]
        /*
         * Check here for consistency and add to remove program and then continue if the condition is matched
         * Check if the given results are "Consistent" with the positiveExamples
         * For each positive examples check if it exists given a result from uniqueResults[]
         */
        if (!isConsistent(result.result, positiveExamples, isScalar)) {
            //Add the result's index to removeIndex[], because we want to remove this one
            removeIndex.push(i)
        }
    }

    //The actual removing is done here
    //Sort descending (biggest to smallest)
    removeIndex = removeIndex.sort(function(a, b){return b-a} );
    removeIndex.forEach(function(index){
        uniqueResults.splice(index, 1)
    })

    removeIndex = []
    var newUniqueResults = []

    //Get the generic ones based on the subsuming definition
    for(var i=0; i<uniqueResults.length; i++){
        var include = true;
        var result = uniqueResults[i]

        //Check if the result is a subset of another program result, then don't include prog
        for(var k=i+1; k<uniqueResults.length; k++){
            var resultk = uniqueResults[k]
            if( Util.isSubsume(resultk.result, result.result )   &&
                (!Util.isSubsume(result.result , resultk.result ) || k<i) &&
                _.find(removeIndex, k)==undefined //Make sure that it is not a previous program or the program that you are comparing against (k) isn't already removed
            ){
                /*
                 * If result is a subset of resultk...
                 * and make sure that resultk is not a subset of result
                 */
                include = false
                removeIndex.push(i)
                break;
            }
        }
        if(include) newUniqueResults.push(uniqueResults[i])
    }

    //Concat all programs in uniqueResults[] into the array tot[]
    var plucked = _.pluck(newUniqueResults, 'listOfItems');
    var tot = _.reduce(plucked, function(tot, elem){
        return tot.concat(elem)
    })
    Util.logTime(21, "END: CleanUp")

    //Util.logLearn("CleanUp() time:" + Util.getTimeDifference(21))
    //Util.logLearn("END CleanUp() length:" + tot.length)

    return tot

}
exports.CleanUpGeneric = CleanUpGeneric;


//Check if the posExamples somehow exists in test[]
function isConsistent(test, posExamples, isScalar){

    if(Util.isEmpty(test) || Util.isEmpty(posExamples)){
        return undefined
    }
    /* The consistency check is different for Pair operator because it is a scalar program (returns a scalar)
     * The other three operators are sequence programs (return a sequence)
     * (See Definition of Consistency)
     */
    if(!isScalar){
        if(Util.isSubsume(posExamples, test)){
            return true
        }else{
            return false
        }
    }else{
        //posExamples[] must be equal to test[]
        var isConsistent = (_.intersection(test, posExamples).length == posExamples.length)
        return isConsistent
    }

}
exports.isConsistent = isConsistent;

/* Sort out the programs based on results. So we have a set of unique results and
 * each result is associated with a list of programs: {'result':result, 'listofprogs':[]}
 * IOW, Executes the programs given by the executionFunction and organizes the all the programs by result
 */
function organizeByResult(programs, executionFunction){

    //Util.logLearn("Start: Organize() length:" + programs.length)

    Util.logTime(22, "START: Organize")

    //Organize the prog based on its execution
    var uniqueResults = []

    //Populate result[] with results to execution of the programs on the whole region
    for(var i=0; i<programs.length; i++){

        var prog = programs[i]
        var result = executionFunction(prog)

        //Find the result in uniqueResult
        var find = _.find(uniqueResults, function(elem){return _.isEqual(elem.result,result)})

        if(find!=undefined){   //If the result is found in uniqueResults[], add the program to the appropriate place

            //Find the result in uniqueResult[]
            for(var j=0; j<uniqueResults.length; j++){
                if(_.isEqual(uniqueResults[j].result, result)){
                    uniqueResults[j].listofprogs.push(prog)
                    break;
                }
            }

        }else{      //If the result could not be found in uniqueResults, then add it to unique result
            uniqueResults.push({'result': result, 'length':result.length ,'listofprogs': [prog]})
         }
    }

    uniqueResults = (_.sortBy(uniqueResults, 'length'))

    Util.logTime(22, "END: Organize")

    //Util.logLearn("Organize() time:" + Util.getTimeDifference(22))
    //Util.logLearn("END Organize() length:" + uniqueResults.length)

    return uniqueResults

}
exports.organizeByResult = organizeByResult;

//Same as above except we dont have to execute for the result. It is already given
function organizeByGivenResult(array){

    //Util.logLearn("Start: Organize() length:" + array.length)

    Util.logTime(23, "START: Organize")

    //Organize the prog based on its execution
    var uniqueResults = []

    //Populate result[] with results to execution of the programs on the whole region
    for(var i=0; i<array.length; i++){
        var item = array[i]
         var result = array[i].result

        //Find the result in uniqueResult
        var find = _.find(uniqueResults, function(elem){return _.isEqual(elem.result,result)})

        if(find!=undefined){   //If the result is found in uniqueResults[], add the program to the appropriate place

            //Find the result in uniqueResult[]
            for(var j=0; j<uniqueResults.length; j++){
                if(_.isEqual(uniqueResults[j].result, result)){
                    //delete item.result
                    item = DeleteKey(item, ['result'])
                    uniqueResults[j].listOfItems.push(item)
                    break;
                }
            }

        }else{      //If the result could not be found in uniqueResults, then add it to unique result
            item = DeleteKey(item, ['result'])
            uniqueResults.push({'result': result, 'length':result.length ,'listOfItems':[item]})
        }
    }

    uniqueResults = (_.sortBy(uniqueResults, 'length'))

    Util.logTime(23, "END: Organize")

    //Util.logLearn("Organize() time:" + Util.getTimeDifference(23))
    //Util.logLearn("END Organize() length:" + uniqueResults.length)

    return uniqueResults

}
exports.organizeByGivenResult = organizeByGivenResult;
