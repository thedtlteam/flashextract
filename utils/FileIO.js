/********************************************************************************
     FileIO for debugging purposes
 ********************************************************************************/
var _ = require('lodash');

//Make a array of arrays readable in text file
function toString(arrayofarray){
    var str="";
    arrayofarray.forEach(function(array) {
        str = str + JSON.stringify(array) + "\n";
    });
    return str;
}
exports.toString = toString;

//For analysing long results, write results to a file
function writeToFile(filename,contents ){
    var fs = require('fs');
    fs.writeFile(__dirname+"/../debug/"+filename, contents, function(err) {
        if(err) {
            console.log(err);
        } else {
            console.log("The file was saved:"+filename);
        }
    });
}
exports.writeToFile = writeToFile;

//Given an array of objects, JSONify them and put newline in between all the keys
function newlineJSON(array){
    var str="";
    array.forEach(function(elem) {
        str=str+'{'
       _.forIn(elem, function(value, key){str=str+" \'"+ key+"\':"+JSON.stringify(value)+"\n"})
        str = str + '}\n'
    });
    return str;
}
exports.newlineJSON = newlineJSON;